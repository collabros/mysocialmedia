# Projet Tutoré - Gestionnaire de Médiathèque en ligne

## Membres du groupes 
- Jordan Becker
- Jacques Cornat
- Thomas Frantz
- Julien Michel

##Informations importantes
- Faire une copie du fichier config/config.ini.default en config/config.ini, et le remplir avec les identifiants de sa base de données
- Pensez bien à créer une base de donnée vide sur votre MySQL (choisir le m¤me nom que l'argument "dbname" dans le config.ini que vous avez créé)
- Après chaque pull, ou chaque modification des entités, exécuter cette commande : "php vendor/bin/doctrine orm:schema-tool:update --force"
- Pour générer les getters/setters après la création d'une entité, exécuter la commande : "php vendor/bin/doctrine orm:generate:entities ."
- Pour générer les repositories après la création d'une entité, exécuter la commande : "php vendor/bin/doctrine orm:generate:repositories ."
- Le dashboard du projet est accessible ici : https://docs.google.com/spreadsheets/d/1H7l9-Y9Vieq4sQQCH-japzSTbcfvOzEHGmexdb5r7lk/edit

## Technologies
- Composer
- [Silex (Micro Framework)](https://packagist.org/packages/silex/silex)
- [Twig (Templating)](https://packagist.org/packages/twig/twig)
- [Doctrine (ORM)](http://www.doctrine-project.org/)
- [BootStrap (Design)](http://getbootstrap.com/)