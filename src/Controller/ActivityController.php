<?php

namespace src\Controller;

use src\AbstractController;
use src\Entity\Activity;
use Symfony\Component\Validator\Constraints as Assert;

class ActivityController extends AbstractController {

    public function getLatest($memberId) {
        $data = array();

        $data['activities'] = [];

        try {

            if($_SESSION['id'] != $memberId && !isset($_SESSION['admin'])) {
                throw new \Exception("Forbidden");
            }

            $member = $this->em->getRepository('src\Entity\Member')->find($memberId);

            if ($member == null) {
                throw new \Exception('Member not found');
            }


            $contacts = $member->getContacts();

            if(sizeof($contacts) == 0) {
                return $this->app->json($data, 200);
            }

            $qb = $this->em->createQueryBuilder();

            $qb->select('a')
                ->from('src\Entity\Activity', 'a')
                ->orderBy('a.createdAt', 'DESC');

            $or = $qb->expr()->orX();

            foreach($contacts as $key => $value){
                $or->add($qb->expr()->eq("a.member", "?{$key}"));
                $qb->setParameter($key, $value->getContact()->getId());
            }
            $qb->where($or);
            $qb->setMaxResults(10);

            $results = $qb->getQuery()->execute();
            foreach ($results as $activity) {
                $a = [];
                $a['member'] = $activity->getMember()->toArrayShort();

                if($activity->getMedia() != null) {
                    $a['media'] = $activity->getMedia()->toArray();
                }
                if($activity->getContact() != null) {
                    $a['contact'] = $activity->getContact()->toArrayShort();
                }
                if($activity->getDocument() != null) {
                    $a['document'] = $activity->getDocument()->toArray();
                }

                $a['label'] = $activity->getActivityLabel()->getName();

                $data['activities'][] = $a;
            }

        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 200);
        }

        return $this->app->json($data, 200);
    }

    public function addActivity($m, $o, $labelId) {

        try {
            $activity = new Activity();
            $member = $this->em->getRepository('src\Entity\Member')->find($m->getId());
            $activity->setMember($member);

            $label = $this->em->getRepository('src\Entity\ActivityLabel')->find($labelId);
            $activity->setActivityLabel($label);

            $prefix = "src\\Entity\\";
            switch(get_class($o)) {
                case $prefix."Document":
                    $document = $this->em->getRepository('src\Entity\Document')->find($o->getId());
                    $activity->setDocument($document);
                    break;
                case $prefix."Member":
                    $contact = $this->em->getRepository('src\Entity\Member')->find($o->getId());
                    $activity->setContact($contact);
                    break;
                case $prefix."Media":
                    $media = $this->em->getRepository('src\Entity\Media')->find($o->getId());
                    $activity->setMedia($media);
                    break;
                default:
                    throw new \Exception("Class not found : ".get_class($o));
            }

            $this->em->persist($activity);
            $this->em->flush();

        } catch (\Exception $e) {
            error_log($e->getMessage());
        }
    }

}