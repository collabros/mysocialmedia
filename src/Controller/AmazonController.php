<?php


namespace src\Controller;

use ApaiIO\ApaiIO;
use ApaiIO\Operations\Lookup;
use src\AbstractController;
use src\Entity\Author;
use src\Entity\Band;
use src\Entity\Document;
use src\Entity\Member;
use Symfony\Component\Validator\Constraints as Assert;
use ApaiIO\Configuration\GenericConfiguration;
use ApaiIO\Operations\Search;

class AmazonController extends AbstractController {

    public function search(){
        $request = $this->request->query->all();
        $data = array();
        $ini = parse_ini_file("config/config.ini");
        $conf = new GenericConfiguration();
        $conf->setCountry('fr')
            ->setAccessKey($ini['AWS_API_KEY'])
            ->setSecretKey($ini['AWS_API_SECRET_KEY'])
            ->setAssociateTag($ini['AWS_ASSOCIATE_TAG']);
        $search = new Search();
        if(!isset($request['filter'])||!isset($request['search'])){
            throw new \Exception("No filter or search query provided");
        }
        try{
            switch($request['filter']){
                // ALL
                case 0:
                    $search->setKeywords($request['search']);
                    break;
                // Livres
                case 10:
                    $search->setCategory('Books')
                        ->setKeywords($request['search']);
                    break;
                // Livre -> Titre
                case 11:
                    $search->setCategory('Books')
                        ->setTitle($request['search']);
                    break;
                // Livre -> Auteur
                case 12:
                    $search->setCategory('Books')
                        ->setAuthor($request['search']);
                    break;
                // Livre -> Collection
                case 13:
                    $search->setCategory('Books')
                        ->setKeywords($request['search']);
                    break;
                // Livre -> Edition
                case 14:
                    $search->setCategory('Books')
                        ->setKeywords($request['search']);
                    break;
                // Livre -> ISBN
                case 15:
                    $search->setCategory('Books')
                        ->setKeywords($request['search']);
                    break;
                // JV
                case 20:
                    $search->setCategory('VideoGames')
                        ->setKeywords($request['search']);
                    break;
                // JV -> Title
                case 21:
                    $search->setCategory('VideoGames')
                        ->setTitle($request['search']);
                    break;
                // JV -> Studio
                case 22:
                    $search->setCategory('VideoGames')
                        ->setManufacturer($request['search']);
                    break;
                // JV -> Producteur
                case 23:
                    $search->setCategory('VideoGames')
                        ->setAuthor($request['search']);
                    break;
                // JV -> Console
                case 24:
                    $search->setCategory('VideoGames')
                        ->setBrand($request['search']);
                    break;
                // Musique
                case 30:
                    $search->setCategory('Music')
                        ->setKeywords($request['search']);
                    break;
                // Musique -> Title
                case 31:
                    $search->setCategory('Music')
                        ->setTitle($request['search']);
                    break;
                // Musique  -> Album
                case 32:
                    $search->setCategory('Music')
                        ->setKeywords($request['search']);
                    break;
                // Musique  -> Groupe
                case 33:
                    $search->setCategory('Music')
                        ->setKeywords($request['search']);
                    break;
                // Musique  -> Musicien
                case 34:
                    $search->setCategory('Music')
                        ->setArtist($request['search']);
                    break;
                // DVD
                case 40:
                    $search->setCategory('DVD')
                        ->setKeywords($request['search']);
                    break;
                // DVD -> Titre
                case 41:
                    $search->setCategory('DVD')
                        ->setTitle($request['search']);
                    break;
                // DVD -> Acteur
                case 42:
                    $search->setCategory('DVD')
                        ->setActor($request['search']);
                    break;
                // DVD -> Producteur
                case 43:
                    $search->setCategory('DVD')
                        ->setDirector($request['search']);
                    break;
                default:
                    throw new \Exception("Undefined filter");

            }
            if(isset($request['page'])){
                if(is_int(intval($this->request->get('page'))) && $this->request->get('page') > 0 && $this->request->get('page') < 11){
                    $data['page'] = $this->request->get('page');
                    $search->setPage($this->request->get('page'));
                    if($this->request->get('page') <1 || $this->request->get('page') > 10){
                        $data = array();
                        $data['errors'] = "Page Number must be contained in 1 and 10";
                        return $this->app->json($data,400);
                    }
                }
                else {
                    $data['page'] = 1;
                    $search->setPage(1);
                }
            }
            else {
                $data['page'] = 1;
            }
            $apaiIo = new ApaiIO($conf);

            $asin = "";
            $totalItems = 0;
            $lookup = new Lookup();
            $lookup->setResponseGroup(array('Large'));


            if ($request['filter'] == 0){
                $search->setCategory('Books');
                $itemsList = array();
                $itemsList['Books'] = simplexml_load_string($apaiIo->runOperation($search));
                $search->setCategory('DVD');
                $itemsList['DVD'] = simplexml_load_string($apaiIo->runOperation($search));
                $search->setCategory('VideoGames');
                $itemsList['VideoGames'] = simplexml_load_string($apaiIo->runOperation($search));
                $search->setCategory('Music');
                $itemsList['Music'] = simplexml_load_string($apaiIo->runOperation($search));
                foreach ($itemsList as $kind) {
                    $res = array();
                    $asin = "";
                    $totalItems += $kind->Items->TotalResults;
                    foreach ($kind->Items->Item as $item) {
                        if ($asin == ""){
                            $asin = "" . $item->ASIN;
                        }else {
                            $asin = $asin .','. $item->ASIN;
                        }
                    }
                    $lookup->setItemId($asin);
                    $lk = $apaiIo->runOperation($lookup);
                    $items = simplexml_load_string($lk)->Items->Item;
                    foreach ($items as $item) {
                        array_push($res,$item);
                    }

                    $data[''. $kind->Items->Request->ItemSearchRequest->SearchIndex] = $res;
                    $data['totalitems'] =$totalItems;

                }

            }else{
                $xml = $apaiIo->runOperation($search);
                $itemsList = simplexml_load_string($xml);
                $totalItems = $itemsList->Items->TotalResults;
                $res = array();
                foreach ($itemsList->Items->Item as $item) {
                    if ($asin == ""){
                        $asin = "" . $item->ASIN;
                    }else {
                        $asin = $asin .','. $item->ASIN;
                    }
                }
                $lookup->setItemId($asin);
                $lk = $apaiIo->runOperation($lookup);
                $items = simplexml_load_string($lk)->Items->Item;
                foreach ($items as $item) {
                    array_push($res,$item);
                }
                $data[''.$itemsList->Items->Request->ItemSearchRequest->SearchIndex] = $res;
                $data['totalitems'] =$totalItems;
            }
        }catch (\Exception $e){
            $data = ['errors'=>$e->getMessage()];
            return $this->app->json($data,500);
        }
        return $this->app->json($data,200);
    }

    public function getProductByASIN($asin) {
        $data = array();

        try {

            $ini = parse_ini_file("config/config.ini");
            $conf = new GenericConfiguration();
            $conf->setCountry('fr')
                ->setAccessKey($ini['AWS_API_KEY'])
                ->setSecretKey($ini['AWS_API_SECRET_KEY'])
                ->setAssociateTag($ini['AWS_ASSOCIATE_TAG']);

            $apaiIo = new ApaiIO($conf);
            $lookup = new Lookup();
            $lookup->setResponseGroup(array('Large'));

            $lookup->setItemId($asin);
            $lk = $apaiIo->runOperation($lookup);

            $item = simplexml_load_string($lk)->Items->Item;
            $document = new Document();
            switch ($item->ItemAttributes->ProductGroup) {
                case "Video Games":
                    $documentType = $this->em->getRepository('src\Entity\DocumentType')->find(2);
                    break;

                case "Book":
                    $documentType = $this->em->getRepository('src\Entity\DocumentType')->find(1);
                    $authorName = $item->ItemAttributes->Author;
                    $author = $this->em->getRepository('src\Entity\Author')->findOneByName($authorName);
                    if ($author != null) {
                        $document->addAuthor($author);
                    } else {
                        $author = new Author();
                        $author->setName($authorName);
                        $this->em->persist($author);
                        $document->addAuthor($author);
                    }
                    break;

                case "DVD":
                    $documentType = $this->em->getRepository('src\Entity\DocumentType')->find(3);
                    $authorName = $item->ItemAttributes->Director;
                    $author = $this->em->getRepository('src\Entity\Author')->findOneByName($authorName);
                    if ($author != null) {
                        $document->addAuthor($author);
                    } else {
                        $author = new Author();
                        $author->setName($authorName);
                        $this->em->persist($author);
                        $document->addAuthor($author);
                    }
                    break;

                case "Music":
                    $documentType = $this->em->getRepository('src\Entity\DocumentType')->find(4);
                    $artistName = $item->ItemAttributes->Artist;
                    $band = $this->em->getRepository('src\Entity\Band')->findOneByName($artistName);
                    if ($band != null) {
                        $document->setBand($band);
                    } else {
                        $band = new Band();
                        $band->setName($artistName);
                        $this->em->persist($band);
                        $document->setBand($band);
                    }
                    break;

                default:
                    throw new \Exception("Bad GroupType");
                    break;
            }

            $checkDocument = $this->em->getRepository('src\Entity\Document')
                ->findOneBy(array('ean13' => $item->ItemAttributes->EAN));

            if ($checkDocument != null) {
                $data['result'] = $checkDocument->toArray();
                return $this->app->json($data, 200);
            }

            $document->setType($documentType);
            $document->setName($item->ItemAttributes->Title);
            $document->setEan13($item->ItemAttributes->EAN);
            if (property_exists($item->ItemAttributes, 'ISBN')) {
                $document->setIsbn($item->ItemAttributes->ISBN);
            }
            $document->setSmallImage($item->SmallImage->URL);
            $document->setLargeImage($item->LargeImage->URL);

            $this->em->persist($document);
            if (isset($author)) {
                $author->addDocument($document);
                $this->em->persist($author);
            }
            if (isset($band)) {
                $band->addDocument($document);
                $this->em->persist($band);
            }
            $this->em->flush();
            $data['result'] = $document->toArray();
        } catch (\Exception $e) {
            $data = ['errors' => $e->getMessage()];
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

}