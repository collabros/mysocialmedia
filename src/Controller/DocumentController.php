<?php


namespace src\Controller;

use src\AbstractController;
use src\Entity\Document;
use src\Entity\DocumentComment;
use Symfony\Component\Validator\Constraints as Assert;

class DocumentController extends AbstractController {

    public function get($id = null) {
        $data = array();
        try {
            $data['documents'] = [];
            if ($id === null) {
                $documents = $this->em->getRepository('src\Entity\Document')->findAll();
                foreach ($documents as $document) {
                    $data['documents'][] = $document->toArray();
                }
            } else {
                $document = $this->em->find('src\Entity\Document', $id);
                if($document == null) {
                    throw new \Exception('Document not found');
                }
                $data['documents'] = $document->toArray();
            }
        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function getLatest() {
        $data = array();

        try {
            $data['documents'] = [];

            $qb = $this->em->createQueryBuilder();

            $qb->select('d')
                ->from('src\Entity\Document','d')
                ->orderBy('d.createdAt', 'DESC');

            $qb->setMaxResults(24);

            $results = $qb->getQuery()->execute();
            foreach ($results as $document) {
                $data['documents'][] = $document->toArray();
            }
        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function add() {
        $data = array();
        $body = $this->request->getContent();
        $param = json_decode($body);

        try {
            $errors = array();
            $document = new Document();
            $this->sanitize($param, $document, $errors);

            if(sizeof($errors) > 0) {
                throw new \Exception();
            }

            $this->em->persist($document);
            $this->em->flush();
        } catch(\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function edit($id) {
        $data = array();
        $body = $this->request->getContent();
        $param = json_decode($body);

        try {
            $errors = array();
            $document = $this->em->getRepository('src\Entity\Document')->find($id);

            if($document == null) {
                throw new \Exception("Document not found");
            }

            $this->sanitize($param, $document, $errors);

            $type = $this->em->getRepository('src\Entity\DocumentType')->find($param->typeId);

            if($type == null) {
                throw new \Exception("DocumentType not found");
            }

            $document->setType($type);

            if(sizeof($errors) > 0) {
                throw new \Exception();
            }

            $this->em->persist($document);
            $this->em->flush();
        } catch(\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function delete($id) {
        $data = array();

        try {
            $document = $this->em->getRepository('src\Entity\Document')->find($id);

            if($document == null) {
                throw new \Exception("Document not found");
            }

            $this->em->remove($document);
            $this->em->flush();
        } catch(\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    // Document comments //
    public function getComment($id_document, $id_comment = null) {
        $data = array();
        try {
            $this->checkDocument($id_document);

            $data['comments'] = array();

            if ($id_comment == null) {
                $comments = $this->em->getRepository('src\Entity\DocumentComment')->findBy(array('document' => $id_document));
                if ($comments == null) {
                    return $this->app->json($data, 200);
                }

                foreach ($comments as $comment) {
                    $data['comments'][] = $comment->toArray();
                }
            } else {
                $comment = $this->em->getRepository('src\Entity\DocumentComment')->findOneBy(array('document' => $id_document, 'id' => $id_comment));
                if ($comment == null) {
                    throw new \Exception("Comment not found");
                }
                $data['comments'] = $comment->toArray();
            }

        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function getLatestComments($id_document) {
        $data = array();

        try {
            $param = $this->checkJson($this->request->getContent());
            $data['comments'] = [];

            $qb = $this->em->createQueryBuilder();

            $qb->select('c')
                ->from('src\Entity\DocumentComment', 'c')
                ->where('c.document = :iddocument')
                ->orderBy('c.createdAt', 'DESC')
                ->setParameter(':iddocument', $id_document);


            if (array_key_exists('count', $param) && $param->count != null) {
                $qb->setMaxResults($param->count);
            } else {
                $qb->setMaxResults(10);
            }

            if (array_key_exists('from', $param) && $param->from != null) {
                $qb->setFirstResult($param->from);
            }

            $results = $qb->getQuery()->execute();
            foreach ($results as $comment) {
                $data['comments'][] = $comment->toArray();
            }
        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function addComment($id_document) {
        $data = array();
        try {
            $param = $this->checkRequest(array('message'));
            $message = $param->message;

            $document = $this->checkDocument($id_document);

            $id_author = $_SESSION['id'];
            $author = $this->em->getRepository('src\Entity\Member')->find($id_author);
            if ($author == null) {
                throw new \Exception('Member not found');
            }

            $comment = new DocumentComment();
            $comment->setDocument($document);
            $comment->setAuthor($author);
            $comment->setMessage($message);

            $this->em->persist($comment);
            $this->em->flush();

        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function deleteComment($id_document, $id_comment) {
        $data = array();
        try {
            $this->checkDocument($id_document);


            $comment = $this->em->getRepository('src\Entity\DocumentComment')->find($id_comment);

            if ($comment == null) {
                throw new \Exception('Comment not found');
            }

            if($_SESSION['id'] != $comment->getAuthor()->getId() && !isset($_SESSION['admin'])) {
                throw new \Exception("Forbidden");
            }

            $user = $this->em->getRepository('src\Entity\Member')->find($_SESSION['id']);
            if ($user == null) {
                $data['message'] = "Member not found";
                return $this->app->json($data, 400);
            }

            $this->em->remove($comment);
            $this->em->flush();

        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function editComment($id_document, $id_comment) {
        $data = array();
        try {
            $param = $this->checkRequest(array('message'));
            $message = $param->message;

            $document = $this->checkDocument($id_document);
            if ($document == null) {
                throw new \Exception('Document not found');
            }

            $comment = $this->em->getRepository('src\Entity\DocumentComment')->find($id_comment);
            if ($comment == null) {
                $data['message'] = "Comment not found";
                return $this->app->json($data, 404);
            }

            if($_SESSION['id'] != $comment->getAuthor()->getId() && !isset($_SESSION['admin'])) {
                throw new \Exception("Forbidden");
            }

            $comment->setMessage($message);

            $this->em->persist($comment);
            $this->em->flush();

        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function sanitizeName($value, Document &$object, &$errors) {
        $errors = $this->app['validator']->validateValue($value, new Assert\Length(array('min' => 3)));
        if(count($errors) == 0) {
            $object->setName(htmlspecialchars(filter_var($value), FILTER_SANITIZE_STRING));
        } else {
            error_log(count($errors));
        }
    }

    private function checkDocument($id_document) {
        $document = $this->em->getRepository('src\Entity\Document')->find($id_document);
        if ($document == null) {
            throw new \Exception("Document not found");
        }
        return $document;
    }

    /**
     * Checks if the JSON from the request is well formed, and parses it into an object
     *
     * @param $data Content of the request
     * @return mixed
     * @throws \Exception
     */
    private function checkJson($data) {
        $json = json_decode($data);
        if ($json == null) {
            throw new \Exception("Malformed JSON");
        }
        return $json;
    }

    /**
     * Checks if required parameters are effectively present in a request.
     *
     * @param $data Parsed JSON object
     * @param $params Array, params to check if they are present in the $data
     * @throws \Exception
     */
    private function checkParams($data, $params) {
        foreach ($params as $param) {
            if (!array_key_exists($param, $data)) {
                throw new \Exception("Missing required parameter(s)");
            }
        }
    }

    /**
     * Both checks & parses JSON from the current request, as well as checking the parameters.
     *
     * @param $data Content of the request
     * @param $params Array, params to check if they are present in the $data
     * @return Object
     * @throws \Exception
     */
    private function checkRequest($params) {
        $body = $this->request->getContent();
        $json = $this->checkJson($body);
        $this->checkParams($json, $params);
        return $json;
    }


    public function sanitize($data, &$object, &$errors) {
        $this->sanitizeName($data->name, $object, $errors);
        $this->sanitizeType($data->typeId, $object, $errors);
        $this->sanitizeKinds($data->kinds, $object, $errors);
        $this->sanitizeIsbn($data->isbn, $object, $errors);
        $this->sanitizeEan13($data->ean13, $object, $errors);
        $this->sanitizeAuthors($data->authors, $object, $errors);
        $this->sanitizePublisher($data->publisher, $object, $errors);
        $this->sanitizeEdition($data->edition, $object, $errors);
        $this->sanitizeCollection($data->collection, $object, $errors);
        $this->sanitizeDeveloper($data->developer, $object, $errors);
        $this->sanitizeBand($data->band, $object, $errors);
        $this->sanitizePublishedAt($data->publishedAt, $object, $errors);
    }

}