<?php


namespace src\Controller;

use src\AbstractController;
use src\Entity\DocumentType;
use Symfony\Component\Validator\Constraints as Assert;

class DocumentTypeController extends AbstractController {

    public function get($id = null) {
        $data = array();
        try {
            if ($id === null) {
                $types = $this->em->getRepository('src\Entity\DocumentType')->findAll();
                foreach ($types as $type) {
                    $data['types'][] = $type->toArray();
                }
            } else {
                $type = $this->em->find('src\Entity\DocumentType', $id);
                if($type == null) {
                    throw new \Exception('DocumentType not found');
                }
                $data['types'] = $type->toArray();
            }
        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function add() {
        $data = array();
        $body = $this->request->getContent();
        $param = json_decode($body);

        try {
            $errors = array();
            $type = new DocumentType();
            $this->sanitize($param, $type, $errors);

            if(sizeof($errors) > 0) {
                throw new \Exception();
            }

            $this->em->persist($type);
            $this->em->flush();
        } catch(\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function edit($id) {
        $data = array();
        $body = $this->request->getContent();
        $param = json_decode($body);

        try {
            $errors = array();
            $type = $this->em->getRepository('src\Entity\DocumentType')->find($id);

            if($type == null) {
                throw new \Exception("DocumentType not found");
            }

            $this->sanitize($param, $type, $errors);

            if(sizeof($errors) > 0) {
                throw new \Exception();
            }

            $this->em->persist($type);
            $this->em->flush();
        } catch(\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function delete($id) {
        $data = array();
        try {
            $type = $this->em->getRepository('src\Entity\DocumentType')->find($id);

            if($type == null) {
                throw new \Exception("DocumentType not found");
            }

            $this->em->remove($type);
            $this->em->flush();
        } catch(\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function sanitizeName($firstname, DocumentType &$type) {
        $type->setName(htmlspecialchars(filter_var($firstname), FILTER_SANITIZE_STRING));
    }

    public function sanitize($data, &$type, &$errors) {
        if (isset($data->name) && strlen($data->name) > 0) {
            $this->sanitizeName($data->name, $type);
        } else {
            $errors['name']="Le champ name est obligatoire";
        }
    }

}