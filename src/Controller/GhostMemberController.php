<?php


namespace src\Controller;

use src\AbstractController;
use Symfony\Component\Validator\Constraints as Assert;

class GhostMemberController extends AbstractController {

    public function search(){
        $request = $this->request->query->all();
        $data = [];
        try{
            $member = $this->em->getRepository('src\Entity\Member')->find($request['idmember']);
            if($member == null){
                throw new \Exception('member doesn\'t exist');
            }
            $qb = $this->em->createQueryBuilder();
            $qb->select('a')
                ->from('src\Entity\GhostMember', 'a')
                ->where('a.member = :idmember')
                ->andWhere('a.name LIKE :name')
                ->setParameter('idmember',$member)
                ->setParameter('name','%'.$request['name'].'%');

            $results = $qb->getQuery()->execute();
            foreach ($results as $ghostmember) {
                $data['ghostMembers'][] = $ghostmember->toArray();
            }
        }catch (\Exception $e){
            $data['errors'] = $e->getMessage();
            return $this->app->json($data,500);
        }
        return $this->app->json($data,200);
    }

    public function find($id){
        $data['member'] = [];
        try{
            $ghostMember = $this->em->getRepository('src\Entity\GhostMember')->find($id);
            if($ghostMember == null){
                throw new \Exception('ghostmember doesn\'t exist');
            }
            $data['ghostMember'] = $ghostMember->toArray();
        }catch (\Exception $e){
            $data['errors'] = $e->getMessage();
            return $this->app->json($data,500);
        }
        return $this->app->json($data,200);
    }

}