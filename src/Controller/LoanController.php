<?php

namespace src\Controller;

use src\AbstractController;
use src\Entity\Loan;
use src\Entity\GhostMember;
use Symfony\Component\Validator\Constraints as Assert;

class LoanController extends AbstractController {

    public function get($id = null) {
        $data = array();

        try {
            $data['loans'] = [];
            if($id == null) {
                $idMember = $_SESSION['id'];
                $member = $this->em->getRepository('src\Entity\Member')->find($idMember);
                $borrows = $this->em->getRepository('src\Entity\Loan')->findBy(array("borrower" => $member));
                $lends = $this->em->getRepository('src\Entity\Loan')->findBy(array("lender" => $member));
                foreach ($borrows as $borrow) {
                    $data['loans'][] = $borrow->toArray();
                }

                foreach ($lends as $lend) {
                    $data['loans'][] = $lend->toArray();
                }

            } else {
                $borrow = $this->em->getRepository('src\Entity\Loan')->find($id);
                $data['loans'] = $borrow->toArray();
            }
        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }
        return $this->app->json($data, 200);
    }

    public function getLatest($id) {
        $data = array();

        try {
            $data['loans'] = [];

            if($_SESSION['id'] != $id && !isset($_SESSION['admin'])) {
                throw new \Exception("Forbidden");
            }

            $member = $this->em->getRepository('src\Entity\Member')->find($id);

            if($member == null)  {
                throw new \Exception('Member not found');
            }

            $qb = $this->em->createQueryBuilder();

            $qb->select('l')
                ->from('src\Entity\Loan','l')
                ->orderBy('l.createdAt', 'DESC')
                ->where('l.borrower = ?1')
                ->orWhere('l.lender = ?1')
                ->setParameter(1, $member)
                ->setMaxResults(4);

            $results = $qb->getQuery()->execute();
            foreach ($results as $loan) {
                $data['loans'][] = $loan->toArray();
            }

        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }
        return $this->app->json($data, 200);
    }

    public function add() {
        $data = array();
        $body = $this->request->getContent();
        $param = json_decode($body);

        try {
            $loan = new Loan();

            $errors = array();
            if(!isset($param->status)) {
                throw new \Exception('No status');
            }

            $member = $this->em->getRepository('src\Entity\Member')->find($_SESSION['id']);
            $status = $param->status;
            if($status == "borrow") {
                if(!isset($param->lender)) {
                    throw new \Exception('No lender');
                }
                $loan->setBorrower($member);

                $name = $param->lender->name;
                if(isset($param->lender->id)){
                    $lender = $this->em->getRepository('src\Entity\GhostMember')->find($param->lender->id);
                }else{
                    $lender = $this->em->getRepository('src\Entity\GhostMember')->findOneBy(array("name"=>$name,"member"=>$member));
                }
                if($lender == null) {
                    $lender = new GhostMember();
                    $lender->setName($name);
                    $lender->setMember($member);
                    $this->em->persist($lender);
                }
                $loan->setGhostLender($lender);

            } else if($status == "lend") {
                if(!isset($param->borrower)) {
                    throw new \Exception('No borrower');
                }
                $loan->setLender($member);
                $name = $param->borrower->name;
                if(isset($param->borrower->id)){
                    $borrower = $this->em->getRepository('src\Entity\GhostMember')->find($param->borrower->id);
                }else{
                    $borrower = $this->em->getRepository('src\Entity\GhostMember')->findOneBy(array("name"=>$name,"member"=>$member));
                }
                if($borrower == null) {
                    $borrower = new GhostMember();
                    $borrower->setName($name);
                    $borrower->setMember($member);
                    $this->em->persist($borrower);
                }
                $loan->setGhostBorrower($borrower);

            } else {
                throw new \Exception('Bad status');
            }

            if(!isset($param->documents)) {
                throw new \Exception('No documents');
            }

            foreach ($param->documents as $document) {
                $document = $this->em->getRepository('src\Entity\Document')->find($document->id);
                if($document != null) {
                    $loan->addDocument($document);
                }
            }

            if(sizeof($errors) > 0) {
                throw new \Exception("Errors");
            }

            $this->em->persist($loan);
            $this->em->flush();
        } catch(\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function editOne($id) {
        $data = array();
        $body = $this->request->getContent();
        $param = json_decode($body);

        try {
            $loan = $this->em->getRepository('src\Entity\Loan')->find($id);

            if($loan == null) {
                throw new \Exception('No loan found');
            }

            $errors = array();

            if(!isset($param->status)) {
                throw new \Exception('No status');
            }

            $member = $this->em->getRepository('src\Entity\Member')->find($_SESSION['id']);
            $status = $param->status;
            if($status == "borrow") {
                if(!isset($param->lender)) {
                    throw new \Exception('No lender');
                }
                $loan->setBorrower($member);

                $name = $param->lender->name;
                $lender = $this->em->getRepository('src\Entity\Member')->findOneBy(array('name' => $name));
                if($lender == null) {
                    $lender = new GhostMember();
                    $lender->setName($name);
                    $lender->setMember($member);
                    $this->em->persist($lender);
                    $loan->setGhostLender($lender);
                } else {
                    $loan->setLender($lender);
                }

            } else if($status == "lend") {
                if(!isset($param->borrower)) {
                    throw new \Exception('No borrower');
                }
                $loan->setLender($member);

                $name = $param->borrower->name;
                $borrower = $this->em->getRepository('src\Entity\Member')->findOneBy(array('name' => $name));
                if($borrower == null) {
                    $borrower = new GhostMember();
                    $borrower->setName($name);
                    $borrower->setMember($member);
                    $this->em->persist($borrower);
                    $loan->setGhostBorrower($borrower);
                } else {
                    $loan->setBorrower($borrower);
                }

            } else {
                throw new \Exception('bad status');
            }

            if(!isset($param->documents)) {
                throw new \Exception('No documents');
            }

            foreach ($loan->getDocuments() as $document) {
                $loan->removeDocument($document);
            }

            foreach ($param->documents as $document) {
                $document = $this->em->getRepository('src\Entity\Document')->find($document->id);
                if($document != null) {
                    $loan->addDocument($document);
                }
            }

            if(sizeof($errors) > 0) {
                throw new \Exception("Errors");
            }

            $this->em->persist($loan);
            $this->em->flush();
        } catch(\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function delete($id) {
        $data = array();
        try {
            $loan = $this->em->getRepository('src\Entity\Loan')->find($id);

            if($loan->getLender()) {
                $member = $loan->getLender();
            } else {
                $member = $loan->getBorrower();
            }
            if($_SESSION['id'] != $member->getId() && !isset($_SESSION['admin'])) {
                throw new \Exception("Forbidden");
            }

            if($loan == null) {
                throw new \Exception("Loan not found");
            }

            $this->em->remove($loan);
            $this->em->flush();
        } catch(\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

}