<?php

namespace src\Controller;

use Doctrine\ORM\QueryBuilder;
use src\AbstractController;
use src\Entity\Activity;
use src\Entity\Contact;
use src\Entity\DocumentMember;
use src\Entity\Member;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validation;

class MemberController extends AbstractController {

    public function get($id = null) {
        $data = array();
        try {
            if($id == null) {
                $members = $this->em->getRepository('src\Entity\Member')->findAll();
                foreach ($members as $member) {
                    $data['members'][] = $member->toArray();
                }
            } else {
                $member = $this->em->getRepository('src\Entity\Member')->findOneBy(array('id' => $id));
                if($member == null) {
                    $data['message'] = "Member not found";
                    return $this->app->json($data, 500);
                }

                if($_SESSION['id'] != $id && !isset($_SESSION['admin'])) {
                    $data['members'] = $member->toArrayShort();
                } else {
                    $data['members'] = $member->toArray();
                }
            }

        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function login() {
        $data = array();
        $body = $this->request->getContent();
        $param = json_decode($body);

        if(!isset($param->mail) ||!isset($param->password)) {
            $data['message'] = "Email ou mot de passe manquant";
            return $this->app->json($data, 500);
        }

        $member = $this->em->getRepository('src\Entity\Member')->findOneBy(array('mail' => $param->mail));

        if($member == null) {
            $data['message'] = "Membre inexistant";
            return $this->app->json($data, 500);
        } else {
            if(password_verify($param->password, $member->getPassword())) {
                $_SESSION['id'] = $member->getId();
                $_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
                $_SESSION['userAgent'] = $_SERVER['HTTP_USER_AGENT'];

                if($member->getAdmin()) {
                    $_SESSION['admin'] = true;
                }
            } else {
                $data['message'] = "Mot de passe incorrect";
                return $this->app->json($data, 500);
            }
        }

        $data['infos'] = $member->toArray();
        return $this->app->json($data, 200);
    }

    public function editName($id) {
        $data = array();
        $body = $this->request->getContent();
        $param = json_decode($body);

        try {
            if($_SESSION['id'] != $id && !isset($_SESSION['admin'])) {
                throw new \Exception("Forbidden");
            }

            $member = $this->em->getRepository('src\Entity\Member')->find($id);

            if($member == null) {
                throw new \Exception("Membre inexistant");
            } else {
                $errors = array();
                $this->sanitizeName($param->name, $member, $errors);

                if(sizeof($errors) > 0) {
                    throw new \Exception();
                }

                $this->em->persist($member);
                $this->em->flush();
            }
        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        $data['infos'] = $member->toArray();
        return $this->app->json($data, 200);
    }

    public function editMail($id) {
        $data = array();
        $body = $this->request->getContent();
        $param = json_decode($body);

        try {
            $member = $this->em->getRepository('src\Entity\Member')->find($id);

            if($member == null) {
                throw new \Exception("Membre inexistant");
            } else {
                $errors = array();
                $this->sanitizeMail($param->mail, $member, $errors);

                if(sizeof($errors) > 0) {
                    throw new \Exception();
                }

                $this->em->persist($member);
                $this->em->flush();
            }
        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        $data['infos'] = $member->toArray();
        return $this->app->json($data, 200);
    }

    public function getProfile() {
        $data = array();
        $data['status'] = 200;
        try {
            $member = $this->em->getRepository('src\Entity\Member')->find($_SESSION['id']);
            $data['user'] = $member->toArray();
        } catch (\Exception $e) {
            return $this->app->json($e->getMessage(), 500);
        }
        return $this->app->json($data, 200);
    }

    public function register() {
        $data = array();
        $body = $this->request->getContent();
        $param = json_decode($body);

        try {
            $errors = array();
            $member = new Member();
            $this->sanitize($param, $member, $errors);

            if(sizeof($errors) > 0) {
                throw new \Exception();
            }

            $list = $this->em->getRepository('src\Entity\Member')->findBy(array('mail' => $param->mail));
            if(sizeof($list) > 0) {
                throw new \Exception('Mail déjà utilisé');
            }

            $this->em->persist($member);
            $this->em->flush();

            $_SESSION['id'] = $member->getId();
            $_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
            $_SESSION['userAgent'] = $_SERVER['HTTP_USER_AGENT'];

        } catch(\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);

        }

        $data['infos'] = $member->toArray();
        return $this->app->json($data, 200);
    }

    public function checkSession() {
        $data = array();
        if(array_key_exists('id', $_SESSION)) {
            return $this->app->json($data, 200);
        } else {
            $data['message'] = "Aucune session trouvée";
            return $this->app->json($data, 500);
        }
    }

    public function isAdmin() {
        return $this->app->json(null, 200);
    }

    public function logout() {
        session_destroy();
        session_commit();
        session_start();
        return $this->app->json(null, 200);
    }

    public function isLogged() {
        return $this->app->json(null, 200);
    }

    public function getContacts($id = null) {
        $data = array();
        $data['contacts'] = [];

        try {

            if($id != null && $_SESSION['id'] != $id && !isset($_SESSION['admin'])) {
                throw new \Exception("Forbidden");
            }

            $member = $this->em->getRepository('src\Entity\Member')->find($_SESSION['id']);

            foreach ($member->getContacts() as $contact) {
                $data['contacts'][] = $contact->getContact()->toArrayShort();
            }
        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function getLatestContacts($id) {

        $data = array();

        try {

            if($_SESSION['id'] != $id && !isset($_SESSION['admin'])) {
                throw new \Exception("Forbidden");
            }

            $data['contacts'] = [];

            $member = $this->em->getRepository('src\Entity\Member')->find($id);

            if($member == null)  {
                throw new \Exception('Member not found');
            }

            $qb = $this->em->createQueryBuilder();

            $qb->select('c')
                ->from('src\Entity\Contact','c')
                ->orderBy('c.createdAt', 'DESC')
                ->where('c.contactOf = ?1')
                ->setParameter(1, $member)
                ->setMaxResults(6);

            $results = $qb->getQuery()->execute();
            foreach ($results as $contact) {
                $data['contacts'][] = $contact->getContact()->toArray();
            }

        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function addContact($contactId, $id = null) {
        $data = array();

        try {

            if($id != null && $_SESSION['id'] != $id && !isset($_SESSION['admin'])) {
                throw new \Exception("Forbidden");
            }

            $memberTarget = $this->em->getRepository('src\Entity\Member')->find($contactId);
            if($memberTarget == null) {
                throw new \Exception('Contact not found');
            }

            if($id != null) {
                $member = $this->em->getRepository('src\Entity\Member')->find($id);
            } else {
                $member = $this->em->getRepository('src\Entity\Member')->find($_SESSION['id']);
            }

            if($memberTarget->getId() === $member->getId()) {
                throw new \Exception('Error');
            }

            $contact = new Contact();

            $contact->setContact($memberTarget);
            $contact->setContactOf($member);

            $this->em->persist($contact);
            $this->em->flush();

            $activity = new ActivityController($this->request, $this->app);
            $activity->addActivity($member, $contact, 2);

        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function deleteContact($contactId, $id = null) {
        $data = array();

        try {

            if($id != null && $_SESSION['id'] != $id && !isset($_SESSION['admin'])) {
                throw new \Exception("Forbidden");
            }

            $contact = $this->em->getRepository('src\Entity\Member')->find($contactId);

            if($contact == null) {
                throw new \Exception('Contact not found');
            }

            if($id != null) {
                $member = $this->em->getRepository('src\Entity\Member')->find($id);
            } else {
                $member = $this->em->getRepository('src\Entity\Member')->find($_SESSION['id']);
            }

            if($contact->getId() === $member->getId()) {
                throw new \Exception('Error');
            }
            $qb = $this->em->createQueryBuilder();

            $qb->select('c')
                ->from('src\Entity\Contact','c')
                ->orderBy('c.createdAt', 'DESC')
                ->where('c.contactOf = ?1')
                ->andWhere('c.contact = ?2')
                ->setParameter(1, $member)
                ->setParameter(2, $contact)
                ->setMaxResults(1);

            $result = $qb->getQuery()->execute();
            if($result[0] == null) {
                throw new \Exception('Contact non trouvé');
            }

            $this->em->remove($result[0]);
            $this->em->flush();

        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function getDocuments($id) {
        $data = array();
        $data['documents'] = [];

        try {

            if($_SESSION['id'] != $id && !isset($_SESSION['admin'])) {
                throw new \Exception("Forbidden");
            }

            $member = $this->em->getRepository('src\Entity\Member')->find($id);

            if($member == null) {
                throw new \Exception("Member not found");
            }

            $documentMembers = $this->em->getRepository('src\Entity\DocumentMember')->findBy(array("member" => $member));

            if($documentMembers == null) {
                throw new \Exception("DocumentMember not found");
            }

            foreach ($documentMembers as $documentMember) {
                $data['documents'][] = $documentMember->getDocument()->toArray();
            }

        } catch(\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function addDocument($memberId, $documentId) {
        $data = array();

        try {

            if($_SESSION['id'] != $memberId && !isset($_SESSION['admin'])) {
                throw new \Exception("Forbidden");
            }

            $member = $this->em->getRepository('src\Entity\Member')->find($memberId);

            if($member == null) {
                throw new \Exception("Member not found");
            }

            $document = $this->em->getRepository('src\Entity\Document')->find($documentId);
            if($document == null) {
                throw new \Exception("Document not found");
            }

            $dm = $this->em->getRepository('src\Entity\DocumentMember')->findOneBy(array("document" => $document, "member" => $member));

            if($dm != null) {
                throw new \Exception("DocumentMember already exists");
            }

            $documentMember = new DocumentMember();
            $documentMember->setMember($member);
            $documentMember->setDocument($document);
            $this->em->persist($documentMember);
            $this->em->flush();

            $activity = new ActivityController($this->request, $this->app);
            $activity->addActivity($member, $document, 1);

            $data['documents'] = $document->toArray();

        } catch(\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function removeDocument($memberId, $documentId) {

        $data = array();

        try {

            if($_SESSION['id'] != $memberId && !isset($_SESSION['admin'])) {
                throw new \Exception("Forbidden");
            }

            $member = $this->em->getRepository('src\Entity\Member')->find($memberId);

            if($member == null) {
                throw new \Exception("Member not found");
            }

            $document = $this->em->getRepository('src\Entity\Document')->find($documentId);
            if($document == null) {
                throw new \Exception("Document not found");
            }

            $documentMember = $this->em->getRepository('src\Entity\DocumentMember')->findOneBy(array("document" => $document, "member" => $member));

            if($documentMember == null) {
                throw new \Exception("DocumentMember not found");
            }

            $this->em->remove($documentMember);
            $this->em->flush();

        } catch(\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }


    public function getMines($memberId) {
        $data = array();
        $body = $this->request->getContent();
        $param = json_decode($body);

        try {

            if($_SESSION['id'] != $memberId && !isset($_SESSION['admin'])) {
                throw new \Exception("Forbidden");
            }

            $data['documents'] = [];

            $member = $this->em->getRepository('src\Entity\Member')->find($memberId);

            if($member == null)  {
                throw new \Exception('Member not found');
            }

            $qb = $this->em->createQueryBuilder();

            $qb->select('d')
                ->from('src\Entity\DocumentMember','d')
                ->orderBy('d.createdAt', 'DESC')
                ->where('d.member = ?1')
                ->setParameter(1, $member);

            if(property_exists($param, 'count')) {
                $qb->setMaxResults($param->count);
            } else {
                $qb->setMaxResults(12);
            }

            if(property_exists($param, 'from')) {
                $qb->setFirstResult($param->from);
            }

            $results = $qb->getQuery()->execute();
            foreach ($results as $documentMember) {
                $data['documents'][] = $documentMember->getDocument()->toArray();
            }

            $documentMembers = $this->em->getRepository('src\Entity\DocumentMember')->findBy(array("member" => $member));
            $data['count'] = count($documentMembers);

        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function getLatestMines($memberId) {
        $data = array();

        try {

            if($_SESSION['id'] != $memberId && !isset($_SESSION['admin'])) {
                throw new \Exception("Forbidden");
            }

            $data['documents'] = [];

            $member = $this->em->getRepository('src\Entity\Member')->find($memberId);

            if($member == null)  {
                throw new \Exception('Member not found');
            }

            $qb = $this->em->createQueryBuilder();

            $qb->select('d')
                ->from('src\Entity\DocumentMember','d')
                ->orderBy('d.createdAt', 'DESC')
                ->where('d.member = ?1')
                ->setParameter(1, $member)
                ->setMaxResults(12);

            $results = $qb->getQuery()->execute();
            foreach ($results as $documentMember) {
                $data['documents'][] = $documentMember->getDocument()->toArray();
            }

            $documentMembers = $this->em->getRepository('src\Entity\DocumentMember')->findBy(array("member" => $member));
            $data['count'] = count($documentMembers);

        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function getLatestComments($memberId){
        $data = array();

        try {

            if($_SESSION['id'] != $memberId && !isset($_SESSION['admin'])) {
                throw new \Exception("Forbidden");
            }

            $data['comments'] = [];

            if(!isset($_SESSION['id'])) {
                throw new \Exception('Connexion needed');
            }

            $member = $this->em->getRepository('src\Entity\Member')->find($memberId);

            if($member == null)  {
                throw new \Exception('Member not found');
            }

            $qb = $this->em->createQueryBuilder();

            $qb->select('c')
                ->from('src\Entity\DocumentComment','c')
                ->orderBy('c.createdAt', 'DESC')
                ->where('c.author = ?1')
                ->setParameter(1, $member)
                ->setMaxResults(6);

            $results = $qb->getQuery()->execute();
            foreach ($results as $comment) {
                $data['comments'][] = $comment->toArray();
            }

        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }


    public function sanitizeMail($value, Member &$object, &$errors) {
        $errors = $this->app['validator']->validateValue($value, new Assert\Length(array('min' => 3)));
        if(count($errors) == 0) {
            $object->setMail(htmlspecialchars(filter_var($value), FILTER_SANITIZE_EMAIL));

        } else {
            error_log(count($errors));
        }
    }

    public function sanitizeName($value, Member &$object, &$errors) {
        $errors = $this->app['validator']->validateValue($value, new Assert\Length(array('min' => 3)));
        if(count($errors) == 0) {
            $object->setName(htmlspecialchars(filter_var($value), FILTER_SANITIZE_STRING));

        } else {
            error_log(count($errors));
        }
    }

    public function sanitizePassword($value, Member &$object, &$errors) {
        $errors = $this->app['validator']->validateValue($value, new Assert\Length(array('min' => 5)));
        if(count($errors) == 0) {
            $object->setPassword(password_hash($value, PASSWORD_DEFAULT, array('cost'=> 12)));

        } else {
            error_log(count($errors));
        }
    }

    public function sanitize($data, &$object, &$errors) {
        $this->sanitizeMail($data->mail, $object, $errors);
        $this->sanitizeName($data->name, $object, $errors);
        $this->sanitizePassword($data->password, $object, $errors);

    }
}