<?php

namespace src\Controller;

use src\AbstractController;
use Symfony\Component\Validator\Constraints as Assert;
use Google_Client;
use Google_Http_Request;
use Abraham\TwitterOAuth\TwitterOAuth;
use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphUser;
use src\Entity\Member;

class OAuthController extends AbstractController {

    public function get() {
        try {

            $config = parse_ini_file("config/config.ini");
            $data = array();
            $data['results'] = array();

            /*-----------------------*\
                     Google
            \*-----------------------*/

            // Get the required variables
            // Create a state token to prevent request forgery.
            // Store it in the session for later validation.
            $CLIENT_ID = $config['google_CLIENT_ID'];
            $APPLICATION_NAME = $config['google_APPLICATION_NAME'];
            $state = md5(rand());
            $_SESSION['google_state'] = $state;
            $data['results']['google'] = array();
            $data['results']['google']['CLIENT_ID'] = $CLIENT_ID;
            $data['results']['google']['APPLICATION_NAME'] = $APPLICATION_NAME;
            $data['results']['google']['state'] = $state;

            /*-----------------------*\
                     Twitter
            \*-----------------------*/

            $CLIENT_ID = $config['twitter_CLIENT_ID'];
            $CLIENT_SECRET = $config['twitter_CLIENT_SECRET'];
            $CALLBACK_URL = $config['twitter_CALLBACK_URL'];
            $connection = new TwitterOAuth($CLIENT_ID, $CLIENT_SECRET);
            $request_token = $connection->oauth('oauth/request_token', array('oauth_callback' => $CALLBACK_URL));
            $_SESSION['oauth_token'] = $request_token['oauth_token'];
            $_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];
            $url = $connection->url('oauth/authorize', array('oauth_token' => $request_token['oauth_token']));
            $data['results']['twitter']['url'] = $url;
            $data['results']['twitter']['req'] = $request_token['oauth_token'];;

            /*-----------------------*\
                     Facebook
            \*-----------------------*/

            $CLIENT_ID = $config['facebook_CLIENT_ID'];
            $state = md5(rand());
            $_SESSION['facebook_state'] = $state;
            $data['results']['facebook']['CLIENT_ID'] = $CLIENT_ID;
            $data['results']['facebook']['state'] = $state;
            // Send the client ID, token state, and application name as JSON
            // serving it.

            return $this->app->json($data, 200);
        } catch (\Exception $e) {
            $data = array();
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 404);
        }
    }

    public function signinGoogle() {

        try {
            //We create a Client, according to Google's API
            $config = parse_ini_file("config/config.ini");
            $CLIENT_ID = $config['google_CLIENT_ID'];
            $CLIENT_SECRET = $config['google_CLIENT_SECRET'];
            $client = new Google_Client();
            $client->setClientId($CLIENT_ID);
            $client->setClientSecret($CLIENT_SECRET);
            $client->setRedirectUri('postmessage');

            //We get the POST's Content
            $data = array();
            $body = $this->request->getContent();
            $param = json_decode($body);

            //Check if it's a CSRF attack
            if (!isset($param->state) || $param->state == "") {
                $data['message'] = "State manquant";
                return $this->app->json($data, 500);
            }
            if ($param->state != $_SESSION['google_state']) {
                $data['message'] = "State is different : CSRF detected";
                return $this->app->json($data, 401);
            }

            $code = $param->code;
            // Exchange the OAuth 2.0 authorization code for user credentials.
            $client->authenticate($code);

            $token = json_decode($client->getAccessToken());
            // Verify the token
            $reqUrl = 'https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=' . $token->access_token;
            $req = new Google_Http_Request($reqUrl);

            $tokenInfo = json_decode($client->getIo()->makeRequest($req)->getResponseBody());

            // If there was an error in the token info, abort.
            if (isset($tokenInfo->error)) {
                return $this->app->json($tokenInfo->error, 406);
            }

            // Make sure the token we got is for our app.
            if ($tokenInfo->audience != $CLIENT_ID) {
                return $this->app->json("Token's client ID does not match app's.", 401);
            }

            // Store the token in the session for later use.
            $_SESSION['googleToken'] = json_encode($token);
            $q = 'https://www.googleapis.com/oauth2/v1/userinfo?access_token=' . $token->access_token;
            $json = file_get_contents($q);
            $userInfoArray = json_decode($json, true);

            $memberparam = array();
            $memberparam['email'] = $userInfoArray['email'];
            $memberparam['name'] = $userInfoArray['given_name'];
            $memberparam['urlProfilePicture'] = $userInfoArray['picture'];
            $data = $this->oauthRegister($memberparam);
            return $this->app->json($data,200);

        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 400);
        }
    }

    public function signinTwitter() {
        try {
            //We create a Client, according to Twitter's API
            $config = parse_ini_file("config/config.ini");
            $CLIENT_ID = $config['twitter_CLIENT_ID'];
            $CLIENT_SECRET = $config['twitter_CLIENT_SECRET'];

            $request_token = [];
            $request_token['oauth_token'] = $_SESSION['oauth_token'];
            $request_token['oauth_token_secret'] = $_SESSION['oauth_token_secret'];
            $data = array();
            $body = $this->request->getContent();
            $param = json_decode($body);

            if (isset($param->oauth_token) && $request_token['oauth_token'] !== $param->oauth_token) {
                return $this->app->json("oauth_token is different from the one we sent you to Twitter with",401);
            } else {
                $connection = new TwitterOAuth($CLIENT_ID, $CLIENT_SECRET, $request_token['oauth_token'], $request_token['oauth_token_secret']);
                $access_token = $connection->oauth("oauth/access_token", array("oauth_verifier" => $param->oauth_verifier));
                $_SESSION['twitter_access_token'] = $access_token;
                $connection = new TwitterOAuth($CLIENT_ID, $CLIENT_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
                $user = $connection->get("account/verify_credentials");
                if (isset($user->errors)) var_dump($user->errors); else {
                    $memberparam = array();
                    $memberparam['name'] = $user->name;
                    $memberparam['email'] = $user->screen_name . "@twitter.com";
                    $memberparam['urlProfilePicture'] = $user->profile_image_url;
                    $data = $this->oauthRegister($memberparam);
                    return $this->app->json($data, 200);
                }


            }
        } catch (\Exception $e) {
            $data['message'] = $e;
            return $this->app->json($data, 400);
        }
    }


    public function signinFacebook() {
        try {
            //We create a Client, according to Facebook's API
            $config = parse_ini_file("config/config.ini");
            $CLIENT_ID = $config['facebook_CLIENT_ID'];
            $CLIENT_SECRET = $config['facebook_CLIENT_SECRET'];


            FacebookSession::setDefaultApplication($CLIENT_ID, $CLIENT_SECRET);
            //We get the POST's Content
            $data = array();
            $body = $this->request->getContent();
            $param = json_decode($body);

            //Check if it's a CSRF attack
            if (!isset($param->state) || $param->state == "") {
                $data['message'] = "State manquant";
                return $this->app->json($data, 500);
            }
            if ($param->state != $_SESSION['facebook_state']) {
                $data['message'] = "State is different : CSRF detected";
                return $this->app->json($data, 401);
            }


            $access_token = $param->authResponse->accessToken;
            $session = new FacebookSession($access_token);
            $_SESSION['facebookToken'] = $access_token;

            $user_profile = (new FacebookRequest($session, 'GET', '/me'))->execute()->getGraphObject(GraphUser::className())->asArray();
            $picture = "https://graph.facebook.com/" . $user_profile['id'] . "/picture?width=200&height=200";

            $memberparam = array();
            $memberparam['email'] = $user_profile['email'];
            $memberparam['name'] = $user_profile['first_name'];
            $memberparam['urlProfilePicture'] = $picture;
            $data = $this->oauthRegister($memberparam);
            return $this->app->json($data, 200);
        } catch (\Exception $e) {
            $data['message'] = $e;
            return $this->app->json($data, 400);
        }
    }

    public function oauthRegister($param) {
        try {
            $errors = array();
            $email = $param['email'];
            $name = $param['name'];
            $picture = $param['urlProfilePicture'];

            $member = new Member();

            if (sizeof($errors) > 0) {
                throw new \Exception();
            }

            $list = $this->em->getRepository('src\Entity\Member')->findBy(array('mail' => $email));
            if (sizeof($list) > 0) {
                //Update (or not)
                $data['message'] = "User already registered";
                $member = $list[0];
                $pic = $member->getUrlProfilPicture();
                if ($pic == NULL) {
                    $member->setUrlProfilPicture(htmlspecialchars(filter_var($picture), FILTER_SANITIZE_STRING));
                    $this->em->persist($member);
                    $this->em->flush();
                }
            } else {
                //else create
                $member->setMail(htmlspecialchars(filter_var($email), FILTER_SANITIZE_EMAIL));
                $member->setName(htmlspecialchars(filter_var($name), FILTER_SANITIZE_STRING));
                $member->setUrlProfilPicture(htmlspecialchars(filter_var($picture), FILTER_SANITIZE_STRING));
                $this->em->persist($member);
                $this->em->flush();
                $data['message'] = "Ok";
            }

            $_SESSION['id'] = $member->getId();
            $_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
            $_SESSION['userAgent'] = $_SERVER['HTTP_USER_AGENT'];

            $data['infos'] = $member->toArray();
            return $data;
        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }
    }

}