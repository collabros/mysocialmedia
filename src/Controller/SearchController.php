<?php

namespace src\Controller;

use src\AbstractController;
use src\Entity\Member;
use src\Entity\Group;
use Symfony\Component\Validator\Constraints as Assert;

class SearchController extends AbstractController {

    public function documentSearch() {
        $data = array();
        $data['documents'] = [];

        $search = $this->request->query->get('search');

        $qb = $this->em->createQueryBuilder();

        $qb->select('d')
            ->from('src\Entity\Document', 'd')
            ->where('d.name LIKE :search')
            ->orWhere('d.isbn LIKE :search')
            ->orWhere('d.ean13 LIKE :search')
            ->setParameter(':search', '%'.$search.'%')
            ->setMaxResults(100);

        $documents = $qb->getQuery()->execute();
        $data['b'] = $documents;

        foreach ($documents as $document) {
            $data['documents'][] = $document->toArray();
        }

        return $this->app->json($data, 200);
    }

    public function memberSearch() {
        $data = array();
        $data['members'] = [];

        try {
            $search = $this->request->query->get('search');

            $query = $this->em->createQuery('SELECT m FROM src\Entity\Member m WHERE m.name LIKE :name');
            $query->setParameter('name', '%'.$search.'%');
            $members = $query->getResult();

            $query = $this->em->createQuery('SELECT m FROM src\Entity\GhostMember m WHERE m.name LIKE :name AND m.member = 1');
            $query->setParameter('name', '%'.$search.'%');
            $ghostMembers = $query->getResult();

            foreach ($members as $member) {
                $data['members'][] = $member->toArrayShort();
            }

            foreach ($ghostMembers as $member) {
                $data['ghosts'][] = $member->toArray();
            }
        } catch (\Exception $e) {
            $data['error'] = $e->getMessage();
            return $this->app->json($data, 200);
        }

        return $this->app->json($data, 200);
    }

    public function groupSearch() {
        $data = array();

        $search = $this->request->query->get('search');

        $query = $this->em->createQuery('SELECT g FROM src\Entity\Group g WHERE g.name LIKE :name');
        $query->setParameter('name', '%'.$search.'%');
        $groups = $query->getResult();


        foreach ($groups as $group) {
            $data['groups'][] = $group->toArray();
        }

        return $this->app->json($data, 200);
    }

    public function groupMemberSearch($id_group, $strict=false) {
        $data = array();
        $data['members'] = [];

        try {
            $body = $this->request->getContent();
            $param = json_decode($body);

            if (array_key_exists('name', $param)) {
                $name = $param->name;
            }

            $data['members'] = [];

            $qb = $this->em->createQueryBuilder();

            $qb->select('m')
                ->from('src\Entity\Member','m')
                ->where('m.name like :name')
                ->orderBy('m.createdAt', 'DESC')
                ->setParameter(':name', '%'.$name.'%');

            if(array_key_exists('count', $param) && $param->count != null) {
                $qb->setMaxResults($param->count);
            } else {
                $qb->setMaxResults(10);
            }

            if(array_key_exists('from', $param) && $param->from != null) {
                $qb->setFirstResult($param->from);
            }

            $results = $qb->getQuery()->execute();

            foreach ($results as $member) {

//                $member = new Member();

                $groups=array();

                $memberGroups = $member->getGroups();
                if ($strict) {
                    foreach ($memberGroups as $key=>$group) {
                        if ($group->getGroup()->getId() == $id_group) {
                            $groups[$key] = $group->getGroup()->toArray();
                            $groups[$key]['groupmember'] = $group->toArray();
                            break;
                        }
                    }
                }
                else {
                    foreach ($memberGroups as $key=>$group) {
                        $groups[$key] = $group->getGroup()->toArray();
                        $groups[$key]['groupmember'] = $group->toArray();
                    }
                }

                $data['members'][] = array(
                    'member' => $member->toArray(),
                    'groups' => $groups
                );
            }


        } catch (\Exception $e) {
            $data['error'] = $e->getMessage();
            return $this->app->json($data, 200);
        }

        return $this->app->json($data, 200);
    }

    public function groupBannedSearch($id_group, $strict=false) {
        $data = array();
        $data['members'] = [];

        try {
            $body = $this->request->getContent();
            $param = json_decode($body);

            if (array_key_exists('name', $param)) {
                $name = $param->name;
            }

            $data['members'] = [];

            $qb = $this->em->createQueryBuilder();

            $qb->select('m')
                ->from('src\Entity\Member','m')
                ->where('m.name like :name')
                ->orderBy('m.createdAt', 'DESC')
                ->setParameter(':name', '%'.$name.'%');

            if(array_key_exists('count', $param) && $param->count != null) {
                $qb->setMaxResults($param->count);
            } else {
                $qb->setMaxResults(10);
            }

            if(array_key_exists('from', $param) && $param->from != null) {
                $qb->setFirstResult($param->from);
            }

            $results = $qb->getQuery()->execute();

            foreach ($results as $member) {

                $groups=array();
                $bans=array();

                $memberGroups = $member->getGroups();
                $bannedGroups = $member->getGroupBans();
                if ($strict) {
                    foreach ($memberGroups as $keyg=>$group) {
                        if ($group->getGroup()->getId() == $id_group) {
                            $groups[$keyg] = $group->getGroup()->toArray();
                            $groups[$keyg]['groupmember'] = $group->toArray();

                            $groups[$keyg]['banned'] = false;
                            $groups[$keyg]['groupban'] = null;

                            foreach ($bannedGroups as $keyb => $banned) {
                                if ($banned->getGroup()->getId() == $id_group) {
                                    $groups[$keyg]['banned'] = true;
                                    $groups[$keyg]['groupban'] = $banned;
                                    break;
                                }
                            }

                            break;
                        }
                    }

                    foreach ($bannedGroups as $banned) {
                        if ($banned->getGroup()->getId() == $id_group) {
                            $bans[] = $banned->toArray();
                            break;
                        }
                    }
                }
                else {
                    foreach ($memberGroups as $keyg=>$group) {
                        $groups[$keyg] = $group->getGroup()->toArray();
                        $groups[$keyg]['groupmember'] = $group->toArray();

                        $groups[$keyg]['banned'] = false;
                        $groups[$keyg]['groupban'] = null;

                        foreach ($bannedGroups as $keyb => $banned) {
                            if ($banned->group->getId() == $group->getGroup()->getId()) {
                                $groups[$keyg]['banned'] = true;
                                $groups[$keyg]['groupban'] = $banned;
                                break;
                            }
                        }
                    }
                    foreach ($bannedGroups as $banned) {
                        if ($banned->getGroup()->getId() == $id_group) {
                            $bans[] = $banned->toArray();
                        }
                    }
                }

                $data['members'][] = array(
                    'member' => $member->toArray(),
                    'groups' => $groups,
                    'bans' => $bans,
                );
            }


        } catch (\Exception $e) {
            $data['error'] = $e->getMessage();
            return $this->app->json($data, 200);
        }

        return $this->app->json($data, 200);
    }

}