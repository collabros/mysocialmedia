<?php

namespace src\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping;

/**
 * @Entity(repositoryClass="src\Repository\ActivityRepository")
 * @HasLifecycleCallbacks
 * @Table(name="activities")
 */
class Activity
{

    /**
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Document
     *
     * @ManyToOne(targetEntity="Document", inversedBy="activities")
     */
    protected $document;

    /**
     * @var Media
     *
     * @ManyToOne(targetEntity="Media", inversedBy="activities")
     */
    protected $media;

    /**
     * @var Member
     *
     * @ManyToOne(targetEntity="Member", inversedBy="activities")
     */
    protected $member;

    /**
     * @var Member
     *
     * @ManyToOne(targetEntity="Member")
     */
    protected $contact;

    /**
     * @var ActivityLabel
     *
     * @ManyToOne(targetEntity="ActivityLabel", inversedBy="activities")
     */
    protected $activityLabel;

    /**
     * @var \DateTime
     * @Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    public function __construct() {
        $this->createdAt = new \DateTime("now");
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Activity
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set document
     *
     * @param Document $document
     * @return Activity
     */
    public function setDocument(Document $document = null)
    {
        $this->document = $document;

        return $this;
    }

    /**
     * Get document
     *
     * @return Document 
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Set member
     *
     * @param Member $member
     * @return Activity
     */
    public function setMember(Member $member = null)
    {
        $this->member = $member;

        return $this;
    }

    /**
     * Get member
     *
     * @return Member 
     */
    public function getMember()
    {
        return $this->member;
    }

    /**
     * Set activityLabel
     *
     * @param ActivityLabel $activityLabel
     * @return Activity
     */
    public function setActivityLabel(ActivityLabel $activityLabel = null)
    {
        $this->activityLabel = $activityLabel;

        return $this;
    }

    /**
     * Get activityLabel
     *
     * @return ActivityLabel 
     */
    public function getActivityLabel()
    {
        return $this->activityLabel;
    }

    /**
     * Set contact
     *
     * @param \src\Entity\Member $contact
     * @return Activity
     */
    public function setContact(\src\Entity\Member $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return \src\Entity\Member 
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set media
     *
     * @param \src\Entity\Media $media
     * @return Activity
     */
    public function setMedia(\src\Entity\Media $media = null)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return \src\Entity\Media 
     */
    public function getMedia()
    {
        return $this->media;
    }
}
