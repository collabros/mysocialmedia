<?php

namespace src\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping;

/**
 * @Entity(repositoryClass="src\Repository\ContactRepository")
 * @Table(name="contact")
 */
class Contact
{
    /**
     * @var Member
     *
     * @Id
     * @ManyToOne(targetEntity="Member", inversedBy="contactOf")
     */
    protected $contactOf;

    /**
     * @var Member
     *
     * @Id
     * @ManyToOne(targetEntity="Member", inversedBy="contacts")
     */
    protected $contact;

    /**
     * @var \DateTime
     * @Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    public function __construct() {
        $this->createdAt = new \DateTime("now");
    }


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Contact
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set contactOf
     *
     * @param \src\Entity\Member $contactOf
     * @return Contact
     */
    public function setContactOf(\src\Entity\Member $contactOf)
    {
        $this->contactOf = $contactOf;

        return $this;
    }

    /**
     * Get contactOf
     *
     * @return \src\Entity\Member 
     */
    public function getContactOf()
    {
        return $this->contactOf;
    }

    /**
     * Set contact
     *
     * @param \src\Entity\Member $contact
     * @return Contact
     */
    public function setContact(\src\Entity\Member $contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return \src\Entity\Member 
     */
    public function getContact()
    {
        return $this->contact;
    }
}
