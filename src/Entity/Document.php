<?php

namespace src\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @Entity(repositoryClass="src\Repository\DocumentRepository")
 * @HasLifecycleCallbacks
 * @Table(name="documents")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Document
{
    /**
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @Column(type="string", length=13, nullable=true)
     */
    protected $isbn;

    /**
     * @var string
     *
     * @Column(type="string", length=13, nullable=true)
     */
    protected $ean13;

    /**
     * @var string
     *
     * @Column(type="string", length=255, nullable=true)
     */
    protected $name;

    /**
     * @var Publisher
     *
     * @ManyToOne(targetEntity="Publisher", inversedBy="documents")
     */
    protected $publisher;

    /**
     * @var Collection
     *
     * @ManyToOne(targetEntity="Collection", inversedBy="documents")
     */
    protected $collection;

    /**
     * @var Edition
     *
     * @ManyToOne(targetEntity="Edition", inversedBy="documents")
     */
    protected $edition;

    /**
     * @var DocumentType
     *
     * @ManyToOne(targetEntity="DocumentType", inversedBy="documents")
     */
    protected $type;

    /**
     * @var ArrayCollection
     *
     * @ManyToMany(targetEntity="Loan", mappedBy="documents")
     */
    protected $loans;

    /**
     * @var ArrayCollection
     *
     * @ManyToMany(targetEntity="Kind", inversedBy="documents")
     */
    protected $kinds;

    /**
     * @var ArrayCollection
     *
     * @OneToMany(targetEntity="DocumentMedia", mappedBy="document")
     */
    protected $documentMedias;

    /**
     * @var ArrayCollection
     *
     * @OneToMany(targetEntity="DocumentMember", mappedBy="document")
     */
    protected $documentMembers;

    /**
     * @var ArrayCollection
     *
     * @OneToMany(targetEntity="Activity", mappedBy="document", cascade={"persist", "remove"})
     */
    protected $activities;

    /**
     * @var ArrayCollection
     *
     * @ManyToMany(targetEntity="Author", inversedBy="documents")
     */
    protected $authors;

    /**
     * @var Developer
     *
     * @ManyToOne(targetEntity="Developer", inversedBy="documents")
     */
    protected $developer;

    /**
     * @var Band
     *
     * @ManyToOne(targetEntity="Band", inversedBy="documents")
     */
    protected $band;

    /**
     * @var string
     *
     * @Column(type="string", length=255, nullable=true)
     */
    protected $smallImage;

    /**
     * @var string
     *
     * @Column(type="string", length=255, nullable=true)
     */
    protected $largeImage;

    /**
     * @var \DateTime
     * @Column(type="datetime", name="published_at", nullable=true)
     */
    protected $publishedAt;

    /**
     * @var \DateTime
     * @Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @Column(type="datetime", name="updated_at")
     */
    protected $updatedAt;

    /**
     * @var \DateTime
     * @Column(type="datetime", name="deleted_at", nullable=true)
     */
    protected $deletedAt;

    /**
     * @var ArrayCollection
     *
     * @OneToMany(targetEntity="Wish",mappedBy="document", cascade={"persist","remove"})
     *
     */
    protected $wishes;

    /*
     * @OneToMany(targetEntity="DocumentComment", mappedBy="document", cascade={"remove"})
     **/
    private $documentComments;

    public function __construct() {
        $this->loans = new ArrayCollection();
        $this->kinds = new ArrayCollection();
        $this->activities = new ArrayCollection();
        $this->documentMedias = new ArrayCollection();
        $this->documentMembers = new ArrayCollection();
        $this->wishes = new ArrayCollection();
        $this->createdAt = new \DateTime("now");
        $this->documentComments = new ArrayCollection();
    }

    /**
     * @PrePersist @PreUpdate
     */
    public function onUpdate() {
        $this->updatedAt = new \DateTime("now");
    }

    public function toArray() {
        $a = array(
            'id' => $this->getId(),
            'name' => $this->getName(),
        );

        if($this->getType() != null) {
            $a['typeId'] = $this->getType()->getId();
            $a['type'] = $this->getType()->getName();
        }

        if($this->getSmallImage() != null) {
            $a['smallImage'] = $this->getSmallImage();
        }

        if($this->getLargeImage() != null) {
            $a['largeImage'] = $this->getLargeImage();
        }

        if($this->getEan13() != null) {
            $a['ean'] = $this->getEan13();
        }

        if($this->getIsbn() != null) {
            $a['isbn'] = $this->getIsbn();
        }
        if($this->getAuthors() != null){
            foreach($this->getAuthors() as $author){
                $a['author'] = $author->getName();
            }
        }
        if($this->getBand() != null){
            $a['band'] = $this->getBand()->getName();
        }

        return $a;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isbn
     *
     * @param string $isbn
     * @return Document
     */
    public function setIsbn($isbn)
    {
        $this->isbn = $isbn;

        return $this;
    }

    /**
     * Get isbn
     *
     * @return string 
     */
    public function getIsbn()
    {
        return $this->isbn;
    }

    /**
     * Set ean13
     *
     * @param string $ean13
     * @return Document
     */
    public function setEan13($ean13)
    {
        $this->ean13 = $ean13;

        return $this;
    }

    /**
     * Get ean13
     *
     * @return string 
     */
    public function getEan13()
    {
        return $this->ean13;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Document
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set publishedAt
     *
     * @param \DateTime $publishedAt
     * @return Document
     */
    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    /**
     * Get publishedAt
     *
     * @return \DateTime 
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Document
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Document
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set publisher
     *
     * @param Publisher $publisher
     * @return Document
     */
    public function setPublisher(Publisher $publisher = null)
    {
        $this->publisher = $publisher;

        return $this;
    }

    /**
     * Get publisher
     *
     * @return Publisher 
     */
    public function getPublisher()
    {
        return $this->publisher;
    }

    /**
     * Set collection
     *
     * @param Collection $collection
     * @return Document
     */
    public function setCollection(Collection $collection = null)
    {
        $this->collection = $collection;

        return $this;
    }

    /**
     * Get collection
     *
     * @return Collection 
     */
    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * Set edition
     *
     * @param Edition $edition
     * @return Document
     */
    public function setEdition(Edition $edition = null)
    {
        $this->edition = $edition;

        return $this;
    }

    /**
     * Get edition
     *
     * @return Edition 
     */
    public function getEdition()
    {
        return $this->edition;
    }

    /**
     * Set type
     *
     * @param Type $type
     * @return Document
     */
    public function setType(DocumentType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return DocumentType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set developer
     *
     * @param Developer $developer
     * @return Document
     */
    public function setDeveloper(Developer $developer = null)
    {
        $this->developer = $developer;

        return $this;
    }

    /**
     * Get developer
     *
     * @return Developer 
     */
    public function getDeveloper()
    {
        return $this->developer;
    }

    /**
     * Set band
     *
     * @param Band $band
     * @return Document
     */
    public function setBand(Band $band = null)
    {
        $this->band = $band;

        return $this;
    }

    /**
     * Get band
     *
     * @return Band 
     */
    public function getBand()
    {
        return $this->band;
    }

    /**
     * Add loans
     *
     * @param Loan $loans
     * @return Document
     */
    public function addLoan(Loan $loans)
    {
        $this->loans[] = $loans;

        return $this;
    }

    /**
     * Remove loans
     *
     * @param Loan $loans
     */
    public function removeLoan(Loan $loans)
    {
        $this->loans->removeElement($loans);
    }

    /**
     * Get loans
     *
     * @return Collection
     */
    public function getLoan()
    {
        return $this->loans;
    }

    /**
     * Add kinds
     *
     * @param Kind $kinds
     * @return Document
     */
    public function addKind(Kind $kinds)
    {
        $this->kinds[] = $kinds;

        return $this;
    }

    /**
     * Remove kinds
     *
     * @param Kind $kinds
     */
    public function removeKind(Kind $kinds)
    {
        $this->kinds->removeElement($kinds);
    }

    /**
     * Get kinds
     *
     * @return Collection
     */
    public function getKinds()
    {
        return $this->kinds;
    }

    /**
     * Add authors
     *
     * @param Author $authors
     * @return Document
     */
    public function addAuthor(Author $authors)
    {
        $this->authors[] = $authors;

        return $this;
    }

    /**
     * Remove authors
     *
     * @param Author $authors
     */
    public function removeAuthor(Author $authors)
    {
        $this->authors->removeElement($authors);
    }

    /**
     * Get authors
     *
     * @return Collection
     */
    public function getAuthors()
    {
        return $this->authors;
    }

    /**
     * Get loans
     *
     * @return Collection 
     */
    public function getLoans()
    {
        return $this->loans;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return Document
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set smallImage
     *
     * @param string $smallImage
     * @return Document
     */
    public function setSmallImage($smallImage)
    {
        $this->smallImage = $smallImage;

        return $this;
    }

    /**
     * Get smallImage
     *
     * @return string 
     */
    public function getSmallImage()
    {
        return $this->smallImage;
    }

    /**
     * Set largeImage
     *
     * @param string $largeImage
     * @return Document
     */
    public function setLargeImage($largeImage)
    {
        $this->largeImage = $largeImage;

        return $this;
    }

    /**
     * Get largeImage
     *
     * @return string 
     */
    public function getLargeImage()
    {
        return $this->largeImage;
    }


    /**
     * Add documentMedias
     *
     * @param DocumentMedia $documentMedias
     * @return Document
     */
     public function addDocumentMedia(DocumentMedia $documentMedias)
    {
        $this->documentMedias[] = $documentMedias;
        return $this;
    }

    /**
     * Remove documentMedias
     *
     * @param DocumentMedia $documentMedias
     */
    public function removeDocumentMedia(DocumentMedia $documentMedias)
    {
        $this->documentMedias->removeElement($documentMedias);
    }

    /**
     * Get documentMedias
     *
     * @return Collection 
     */
    public function getDocumentMedias()
    {
        return $this->documentMedias;
    }

    /**
     * Add documentMembers
     *
     * @param DocumentMember $documentMembers
     * @return Document
     */
    public function addDocumentMember(DocumentMember $documentMembers)
    {
        $this->documentMembers[] = $documentMembers;
        return $this;
    }



    /**
     * Remove documentMembers
     *
     * @param DocumentMember $documentMembers
     */
    public function removeDocumentMember(DocumentMember $documentMembers)
    {
        $this->documentMembers->removeElement($documentMembers);
    }

    /**
     * Get documentMembers
     *
     * @return Collection 
     */
    public function getDocumentMembers()
    {
        return $this->documentMembers;
    }

    /**
     * Add activities
     *
     * @param Activity $activities
     * @return Document
     */
    public function addActivity(Activity $activities)
    {
        $this->activities[] = $activities;

        return $this;
    }

    /**
     * Remove activities
     *
     * @param Activity $activities
     */
    public function removeActivity(Activity $activities)
    {
        $this->activities->removeElement($activities);
    }

    /**
     * Get activities
     *
     * @return Collection
     */
    public function getActivities()
    {
        return $this->activities;
    }

    /**
     * Remove wish
     *
     * @param \src\Entity\Wish $wishes
     */
    public function removeWish(\src\Entity\Wish $wishes)
    {
        $this->wishes->removeElement($wishes);
    }

    /**
     * Get wishes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWishes()
    {
        return $this->wishes;
    }

    /**
     * Add wish
     *
     * @param Wish $wish
     * @return Document
     */
    public function addWish(Wish $wish)
    {
        $this->wishes[] = $wish;
        return $this;
    }

    /**
     * Add documentComments
     *
     * @param \src\Entity\DocumentComment $documentComments
     * @return Document
     */
    public function addDocumentComment(\src\Entity\DocumentComment $documentComments)
    {
        $this->documentComments[] = $documentComments;

        return $this;
    }

    /**
     * Remove documentComments
     *
     * @param \src\Entity\DocumentComment $documentComments
     */
    public function removeDocumentComment(\src\Entity\DocumentComment $documentComments)
    {
        $this->documentComments->removeElement($documentComments);
    }

    /**
     * Get documentComments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDocumentComments()
    {
        return $this->documentComments;
    }
}
