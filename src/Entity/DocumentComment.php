<?php
namespace src\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\GeneratedValue;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * @Entity(repositoryClass="src\Repository\DocumentCommentRepository")
 * @HasLifecycleCallbacks
 * @Table(name="document_comment")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class DocumentComment
{

    /**
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @var Document
     *
     *
     * @ManyToOne(targetEntity="Document", inversedBy="documentComments")
     */
    protected $document;


    /**
     * @var \DateTime
     *
     *
     * @Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @Column(type="datetime", name="updated_at")
     */
    protected $updatedAt;

    /**
     * @var \DateTime
     *
     * @Column(type="datetime", name="deleted_at", nullable=true)
     */
    protected $deletedAt;

    /**
     * @var Member
     *
     * @ManyToOne(targetEntity="Member", inversedBy="authorComments")
     */
    protected $author;

    /**
     * @var string
     *
     * @Column(name="message", nullable=true)
     */
    protected $message;


    public function __construct()
    {

    }

    public function toArray()
    {
        return array(
            'id' => $this->getId(),
            'document' => $this->getDocument()->toArray(),
            'author' => $this->getAuthor()->toArray(),
            'createdAt' => $this->getCreatedAt()->format('H:i:s d-m-Y'),
            'updatedAt' => $this->getUpdatedAt()->format('H:i:s d-m-Y'),
            'message' => $this->getMessage()
        );
    }

    /**
     * @PrePersist @PreUpdate
     */
    public function onUpdate()
    {
        $this->updatedAt = new \DateTime("now");
    }

    /**
     * @PrePersist
     */
    public function onCreate()
    {
        $this->createdAt = new \DateTime("now");
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return DocumentComment
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return DocumentComment
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return DocumentComment
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return DocumentComment
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set document
     *
     * @param \src\Entity\Document $document
     * @return DocumentComment
     */
    public function setDocument(\src\Entity\Document $document = null)
    {
        $this->document = $document;

        return $this;
    }

    /**
     * Get document
     *
     * @return \src\Entity\Document 
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Set author
     *
     * @param \src\Entity\Member $author
     * @return DocumentComment
     */
    public function setAuthor(\src\Entity\Member $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \src\Entity\Member 
     */
    public function getAuthor()
    {
        return $this->author;
    }
}
