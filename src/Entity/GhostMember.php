<?php

namespace src\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping;

/**
 * @Entity(repositoryClass="src\Repository\GhostMemberRepository")
 * @HasLifecycleCallbacks
 * @Table(name="ghost_members")
 */
class GhostMember
{
    /**
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @Column(type="string", length=255)
     */
    protected $name;

    /**
     * @var Member
     *
     * @ManyToOne(targetEntity="Member", inversedBy="ghostMembers")
     */
    protected $member;

    /**
     * @var ArrayCollection
     *
     * @ManyToMany(targetEntity="Loan", mappedBy="borrower", cascade={"persist", "remove"})
     */
    protected $borrows;

    /**
     * @var ArrayCollection
     *
     * @OneToMany(targetEntity="Loan", mappedBy="lender", cascade={"persist", "remove"})
     */
    protected $lends;

    /**
     * @var \DateTime
     * @Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @Column(type="datetime", name="updated_at")
     */
    protected $updatedAt;

    public function __construct() {
        $this->createdAt = new \DateTime("now");
    }

    /**
     * @PrePersist @PreUpdate
     */
    public function onUpdate() {
        $this->updatedAt = new \DateTime("now");
    }
    public function toArray() {
        $a = array(
            'id' => $this->getId(),
            'name' => $this->getName(),
            'member' => $this->getMember()->toArray()
        );

        return $a;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return GhostMember
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return GhostMember
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return GhostMember
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set member
     *
     * @param Member $member
     * @return GhostMember
     */
    public function setMember(Member $member = null)
    {
        $this->member = $member;

        return $this;
    }

    /**
     * Get member
     *
     * @return Member
     */
    public function getMember()
    {
        return $this->member;
    }

    /**
     * Add loans
     *
     * @param Loan $loans
     * @return GhostMember
     */
    public function addBorrow(Loan $borrows)
    {
        $this->borrows[] = $borrows;

        return $this;
    }

    /**
     * Remove $loans
     *
     * @param Loan $loans
     */
    public function removeBorrow(Loan $borrows)
    {
        $this->borrows->removeElement($borrows);
    }

    /**
     * Get borrows
     *
     * @return Collection
     */
    public function getBorrows()
    {
        return $this->borrows;
    }

    /**
     * Add lends
     *
     * @param Loan $lends
     * @return GhostMember
     */
    public function addLend(Loan $lends)
    {
        $this->lends[] = $lends;

        return $this;
    }

    /**
     * Remove lends
     *
     * @param Loan $lends
     */
    public function removeLend(Loan $lends)
    {
        $this->lends->removeElement($lends);
    }

    /**
     * Get lends
     *
     * @return Collection
     */
    public function getLends()
    {
        return $this->lends;
    }

}
