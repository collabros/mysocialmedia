<?php

namespace src\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping;

/**
 * @Entity(repositoryClass="src\Repository\GroupRepository")
 * @HasLifecycleCallbacks
 * @Table(name="groups")
 */
class Group
{
    /**
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @var string
     *
     * @Column(type="string", length=255)
     */
    protected $name;

    /**
     * @var GroupType
     *
     * @ManyToOne(targetEntity="GroupType", inversedBy="groups")
     */
    protected $type;

    /**
     * @var ArrayCollection
     *
     * @OneToMany(targetEntity="GroupMember", mappedBy="group", cascade={"persist", "remove"})
     */
    protected $members;

    /**
     * @var \DateTime
     * @Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    /**
     * @OneToMany(targetEntity="GroupBan", mappedBy="group", cascade={"remove"})
     **/
    private $groupBans;

    /**
     * @OneToMany(targetEntity="GroupPost", mappedBy="group", cascade={"remove"})
     **/
    private $groupPosts;

    public function __construct() {
        $this->documents = new ArrayCollection();
        $this->createdAt = new \DateTime("now");
        $this->groupBans= new ArrayCollection();
        $this->groupPosts = new ArrayCollection();
    }

    public function toArray() {
        return array(
            'id' => $this->getId(),
            'type' => $this->getType()->toArray(),
            'name' => $this->getName(),
        );
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Group
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Group
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set type
     *
     * @param GroupType $type
     * @return Group
     */
    public function setType(GroupType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return GroupType 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add members
     *
     * @param GroupMember $members
     * @return Group
     */
    public function addMember(GroupMember $members)
    {
        $this->members[] = $members;

        return $this;
    }

    /**
     * Remove members
     *
     * @param GroupMember $members
     */
    public function removeMember(GroupMember $members)
    {
        $this->members->removeElement($members);
    }

    /**
     * Get members
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * Add groupBans
     *
     * @param \src\Entity\GroupBan $groupBans
     * @return Group
     */
    public function addGroupBan(\src\Entity\GroupBan $groupBans)
    {
        $this->groupBans[] = $groupBans;

        return $this;
    }

    /**
     * Remove groupBans
     *
     * @param \src\Entity\GroupBan $groupBans
     */
    public function removeGroupBan(\src\Entity\GroupBan $groupBans)
    {
        $this->groupBans->removeElement($groupBans);
    }

    /**
     * Get groupBans
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGroupBans()
    {
        return $this->groupBans;
    }

    /**
     * Add groupPosts
     *
     * @param \src\Entity\GroupPost $groupPosts
     * @return Group
     */
    public function addGroupPost(\src\Entity\GroupPost $groupPosts)
    {
        $this->groupPosts[] = $groupPosts;

        return $this;
    }

    /**
     * Remove groupPosts
     *
     * @param \src\Entity\GroupPost $groupPosts
     */
    public function removeGroupPost(\src\Entity\GroupPost $groupPosts)
    {
        $this->groupPosts->removeElement($groupPosts);
    }

    /**
     * Get groupPosts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGroupPosts()
    {
        return $this->groupPosts;
    }
}
