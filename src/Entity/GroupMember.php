<?php

namespace src\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping;

/**
 * @Entity(repositoryClass="src\Repository\GroupMemberRepository")
 * @HasLifecycleCallbacks
 * @Table(name="group_members")
 */
class GroupMember
{
    /**
     * @var Group
     *
     * @Id
     * @ManyToOne(targetEntity="Group", inversedBy="groupMembers")
     */
    protected $group;

    /**
     * @var Member
     *
     * @Id
     * @ManyToOne(targetEntity="Member", inversedBy="groupMembers")
     */
    protected $member;

    /**
     * @var GroupRight
     *
     * @ManyToOne(targetEntity="GroupRight", inversedBy="groupMembers")
     */
    protected $right;

    /**
     * @var \DateTime
     * @Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    public function __construct() {
        $this->documents = new ArrayCollection();
        $this->createdAt = new \DateTime("now");
    }

    public function toArray() {
        return array(
            'member' => $this->getMember()->toArray(),
            'right' => $this->getRight()->toArray(),
            'createdAt' => $this->getCreatedAt()->format('H:i:s d-m-Y'),
        );
    }


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return GroupMember
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set group
     *
     * @param Group $group
     * @return GroupMember
     */
    public function setGroup(Group $group)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return Group 
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set member
     *
     * @param Member $member
     * @return GroupMember
     */
    public function setMember(Member $member)
    {
        $this->member = $member;

        return $this;
    }

    /**
     * Get member
     *
     * @return Member 
     */
    public function getMember()
    {
        return $this->member;
    }

    /**
     * Set right
     *
     * @param GroupRight $right
     * @return GroupMember
     */
    public function setRight(GroupRight $right = null)
    {
        $this->right = $right;

        return $this;
    }

    /**
     * Get right
     *
     * @return GroupRight 
     */
    public function getRight()
    {
        return $this->right;
    }
}
