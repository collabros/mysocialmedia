<?php

namespace src\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping;

/**
 * @Entity(repositoryClass="src\Repository\GroupRightRepository")
 * @HasLifecycleCallbacks
 * @Table(name="group_rights")
 */
class GroupRight
{
    /**
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @Column(type="string", length=255)
     */
    protected $name;

    /**
     * @var integer
     *
     * @Column(type="integer")
     */
    protected $level;

    /**
     * @var ArrayCollection
     *
     * @OneToMany(targetEntity="GroupMember", mappedBy="right", cascade={"persist", "remove"})
     */
    protected $groupMembers;

    /**
     * @var \DateTime
     * @Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @Column(type="datetime", name="updated_at")
     */
    protected $updatedAt;

    public function __construct() {
        $this->documents = new ArrayCollection();
        $this->createdAt = new \DateTime("now");
    }

    public function toArray() {
        return array(
            'id' => $this->getId(),
            'name' => $this->getName(),
            'level' => $this->getLevel()
        );
    }

    /**
     * @PrePersist @PreUpdate
     */
    public function onUpdate() {
        $this->updatedAt = new \DateTime("now");
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return GroupRight
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set level
     *
     * @param integer $level
     * @return GroupRight
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer 
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return GroupRight
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return GroupRight
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add groupMembers
     *
     * @param GroupMember $groupMembers
     * @return GroupRight
     */
    public function addGroupMember(GroupMember $groupMembers)
    {
        $this->groupMembers[] = $groupMembers;

        return $this;
    }

    /**
     * Remove groupMembers
     *
     * @param GroupMember $groupMembers
     */
    public function removeGroupMember(GroupMember $groupMembers)
    {
        $this->groupMembers->removeElement($groupMembers);
    }

    /**
     * Get groupMembers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGroupMembers()
    {
        return $this->groupMembers;
    }
}
