<?php

namespace src\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping;

/**
 * @Entity(repositoryClass="src\Repository\LoanRepository")
 * @HasLifecycleCallbacks
 * @Table(name="loans")
 */
class Loan
{
    /**
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Member
     *
     * @ManyToOne(targetEntity="Member", inversedBy="borrows")
     */
    protected $borrower;

    /**
     * @var Member
     *
     * @ManyToOne(targetEntity="Member", inversedBy="lenders")
     */
    protected $lender;

    /**
     * @var GhostMember
     *
     * @ManyToOne(targetEntity="GhostMember", inversedBy="borrows")
     */
    protected $ghostBorrower;

    /**
     * @var GhostMember
     *
     * @ManyToOne(targetEntity="GhostMember", inversedBy="lenders")
     */
    protected $ghostLender;

    /**
     * @var ArrayCollection
     *
     * @ManyToMany(targetEntity="Document", inversedBy="loans")
     */
    protected $documents;

    /**
     * @var \DateTime
     * @Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @Column(type="datetime", name="finished_at", nullable=true)
     */
    protected $finishedAt;

    public function __construct() {
        $this->documents = new ArrayCollection();
        $this->createdAt = new \DateTime("now");
    }

    public function toArray() {
        $a = array(
            'id' => $this->getId(),
            'createdAt' => $this->getCreatedAt()->format('d-m-Y'),
        );

        foreach ($this->getDocuments() as $document) {
            $a['documents'][] = $document->toArray();
        }

        if($this->getBorrower() != null) {
            $a['borrower'] = $this->getBorrower()->toArrayShort();
        }

        if($this->getLender() != null) {
            $a['lender'] = $this->getLender()->toArrayShort();
        }

        if($this->getGhostBorrower() != null) {
            $a['ghostBorrower'] = $this->getGhostBorrower()->toArray();
        }

        if($this->getGhostLender() != null) {
            $a['ghostLender'] = $this->getGhostLender()->toArray();
        }

        if($this->getFinishedAt() != null) {
            $a['finishedAt'] = $this->getFinishedAt()->format('d-m-Y');
        }

        return $a;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Loan
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set finishedAt
     *
     * @param \DateTime $finishedAt
     * @return Loan
     */
    public function setFinishedAt($finishedAt)
    {
        $this->finishedAt = $finishedAt;

        return $this;
    }

    /**
     * Get finishedAt
     *
     * @return \DateTime 
     */
    public function getFinishedAt()
    {
        return $this->finishedAt;
    }

    /**
     * Set borrower
     *
     * @param Member $borrower
     * @return Loan
     */
    public function setBorrower(Member $borrower = null)
    {
        $this->borrower = $borrower;

        return $this;
    }

    /**
     * Get borrower
     *
     * @return Member 
     */
    public function getBorrower()
    {
        return $this->borrower;
    }

    /**
     * Set lender
     *
     * @param Member $lender
     * @return Loan
     */
    public function setLender(Member $lender = null)
    {
        $this->lender = $lender;

        return $this;
    }

    /**
     * Get lender
     *
     * @return Member 
     */
    public function getLender()
    {
        return $this->lender;
    }

    /**
     * Add documents
     *
     * @param Document $documents
     * @return Loan
     */
    public function addDocument(Document $documents)
    {
        $this->documents[] = $documents;

        return $this;
    }

    /**
     * Remove documents
     *
     * @param Document $documents
     */
    public function removeDocument(Document $documents)
    {
        $this->documents->removeElement($documents);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * Set ghostBorrower
     *
     * @param GhostMember $ghostBorrower
     * @return Loan
     */
    public function setGhostBorrower(GhostMember $ghostBorrower = null)
    {
        $this->ghostBorrower = $ghostBorrower;

        return $this;
    }

    /**
     * Get ghostBorrower
     *
     * @return GhostMember
     */
    public function getGhostBorrower()
    {
        return $this->ghostBorrower;
    }

    /**
     * Set ghostLender
     *
     * @param GhostMember $ghostLender
     * @return Loan
     */
    public function setGhostLender(GhostMember $ghostLender = null)
    {
        $this->ghostLender = $ghostLender;

        return $this;
    }

    /**
     * Get ghostLender
     *
     * @return GhostMember
     */
    public function getGhostLender()
    {
        return $this->ghostLender;
    }
}
