<?php

namespace src\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping;

/**
 * @Entity(repositoryClass="src\Repository\MemberRepository")
 * @HasLifecycleCallbacks
 * @Table(name="members")
 */
class Member {
    /**
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @Column(type="string", length=255)
     */
    protected $name;

    /**
     * @var string
     *
     * @Column(type="string", length=255)
     */
    protected $mail;

    /**
     * @var string
     *
     * @Column(type="string", length=255, nullable=true)
     */
    protected $password;

    /**
     * @var string
     *
     * @Column(type="string", length=255, nullable=true)
     */
    protected $urlProfilPicture;

    /**
     * @var ArrayCollection
     *
     * @OneToMany(targetEntity="Contact", mappedBy="contactOf", cascade={"persist", "remove"})
     */
    protected $contacts;

    /**
     * @var ArrayCollection
     *
     * @OneToMany(targetEntity="Contact", mappedBy="contacts", cascade={"persist", "remove"})
     */
    protected $contactOf;

    /**
     * @var ArrayCollection
     *
     * @OneToMany(targetEntity="Media", mappedBy="member", cascade={"persist", "remove"})
     */
    protected $medias;

    /**
     * @var ArrayCollection
     *
     * @OneToMany(targetEntity="Message", mappedBy="sender", cascade={"persist", "remove"})
     */
    protected $messagesSent;

    /**
     * @var ArrayCollection
     *
     * @OneToMany(targetEntity="Message", mappedBy="recipient", cascade={"persist", "remove"})
     */
    protected $messagesReceived;

    /**
     * @var ArrayCollection
     *
     * @OneToMany(targetEntity="DocumentMember", mappedBy="member")
     */
    protected $documentMembers;

    /**
     * @var ArrayCollection
     *
     * @OneToMany(targetEntity="Activity", mappedBy="member", cascade={"persist", "remove"})
     */
    protected $activities;

    /**
     * @var ArrayCollection
     *
     * @ManyToMany(targetEntity="Loan", mappedBy="borrower", cascade={"persist", "remove"})
     */
    protected $borrows;

    /**
     * @var ArrayCollection
     *
     * @OneToMany(targetEntity="Loan", mappedBy="lender", cascade={"persist", "remove"})
     */
    protected $lends;

    /**
     * @var ArrayCollection
     *
     * @OneToMany(targetEntity="GroupMember", mappedBy="member", cascade={"persist", "remove"})
     */
    protected $groups;

    /**
     * @var ArrayCollection
     *
     * @OneToMany(targetEntity="GroupBan", mappedBy="author", cascade={"persist"})
     */
    protected $authorBans;

    /**
     * @var ArrayCollection
     *
     * @OneToMany(targetEntity="GroupPost", mappedBy="author", cascade={"persist"})
     */
    protected $authorPosts;

    /**
     * @var ArrayCollection
     *
     * @OneToMany(targetEntity="GroupBan", mappedBy="member", cascade={"persist"})
     **/
    private $groupBans;

    /**
     * @var bool
     *
     * @Column(type="boolean", nullable=true)
     */
    protected $admin;

    /**
     * @var ArrayCollection
     *
     * @OneToMany(targetEntity="GhostMember", mappedBy="member", cascade={"persist", "remove"})
     */
    protected $ghostMembers;

    /**
     * @var \DateTime
     * @Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @Column(type="datetime", name="updated_at")
     */
    protected $updatedAt;

    /**
     * @var ArrayCollection
     *
     * @OneToMany(targetEntity="Wish",mappedBy="member", cascade={"persist","remove"})
     *
     */
    protected $wishes;

    /*
     * @OneToMany(targetEntity="DocumentComment", mappedBy="author", cascade={"persist"})
     */
    protected $authorComments;

    public function __construct() {
        $this->medias = new ArrayCollection();
        $this->ghostMembers = new ArrayCollection();
        $this->borrows = new ArrayCollection();
        $this->lends = new ArrayCollection();
        $this->messages = new ArrayCollection();
        $this->contacts = new ArrayCollection();
        $this->contactOf = new ArrayCollection();
        $this->documentMembers = new ArrayCollection();
        $this->activities = new ArrayCollection();
        $this->wishes = new ArrayCollection();
        $this->createdAt = new \DateTime("now");
        $this->authorComments = new ArrayCollection();
        $this->authorBans = new ArrayCollection();
        $this->groupBans = new ArrayCollection();
        $this->authorPosts = new ArrayCollection();
    }

    /**
     * @PrePersist @PreUpdate
     */
    public function onUpdate() {
        $this->updatedAt = new \DateTime("now");
    }

    public function toArray() {
        $a = array(
            'id' => $this->getId(),
            'email' => $this->getMail(),
            'name' => $this->getName(),
            'admin' => $this->getAdmin(),
        );

        if($this->getUrlProfilPicture() != null) {
            $a['profilePicture'] = $this->getUrlProfilPicture();
        } else {
            $a['profilePicture'] =  "http://www.gravatar.com/avatar/".md5($this->getMail())."?d=identicon";
        }

        return $a;
    }

    public function toArrayShort() {
        $a = array(
            'id' => $this->getId(),
            'name' => $this->getName(),
        );

        if($this->getUrlProfilPicture() != null) {
            $a['profilePicture'] = $this->getUrlProfilPicture();
        } else {
            $a['profilePicture'] =  "http://www.gravatar.com/avatar/".md5($this->getMail())."?d=identicon";
        }

        return $a;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Member
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set mail
     *
     * @param string $mail
     * @return Member
     */
    public function setMail($mail) {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string
     */
    public function getMail() {
        return $this->mail;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Member
     */
    public function setPassword($password) {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Member
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Member
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Add medias
     *
     * @param Media $medias
     * @return Member
     */
    public function addMedia(Media $medias) {
        $this->medias[] = $medias;

        return $this;
    }

    /**
     * Remove medias
     *
     * @param Media $medias
     */
    public function removeMedia(Media $medias) {
        $this->medias->removeElement($medias);
    }

    /**
     * Get medias
     *
     * @return Collection
     */
    public function getMedias() {
        return $this->medias;
    }


    /**
     * Get medias
     *
     * @return Collection
     */
    public function getPublicMedias() {
        $a = array();
        foreach ($this->medias as $media) {
            if($media->getPublic() == true) {
                $a[] = $media;
            }
        }

        return $a;
    }

    /**
     * Add messagesSent
     *
     * @param Message $messagesSent
     * @return Member
     */
    public function addMessagesSent(Message $messagesSent) {
        $this->messagesSent[] = $messagesSent;

        return $this;
    }

    /**
     * Remove messagesSent
     *
     * @param Message $messagesSent
     */
    public function removeMessagesSent(Message $messagesSent) {
        $this->messagesSent->removeElement($messagesSent);
    }

    /**
     * Get messagesSent
     *
     * @return Collection
     */
    public function getMessagesSent() {
        return $this->messagesSent;
    }

    /**
     * Add messagesReceived
     *
     * @param Message $messagesReceived
     * @return Member
     */
    public function addMessagesReceived(Message $messagesReceived) {
        $this->messagesReceived[] = $messagesReceived;

        return $this;
    }

    /**
     * Remove messagesReceived
     *
     * @param Message $messagesReceived
     */
    public function removeMessagesReceived(Message $messagesReceived) {
        $this->messagesReceived->removeElement($messagesReceived);
    }

    /**
     * Get messagesReceived
     *
     * @return Collection
     */
    public function getMessagesReceived() {
        return $this->messagesReceived;
    }

    /**
     * Add borrows
     *
     * @param Loan $borrows
     * @return Member
     */
    public function addBorrow(Loan $borrows) {
        $this->borrows[] = $borrows;

        return $this;
    }

    /**
     * Remove borrows
     *
     * @param Loan $borrows
     */
    public function removeBorrow(Loan $borrows) {
        $this->borrows->removeElement($borrows);
    }

    /**
     * Get borrows
     *
     * @return Collection
     */
    public function getBorrows() {
        return $this->borrows;
    }

    /**
     * Add lends
     *
     * @param Loan $lends
     * @return Member
     */
    public function addLend(Loan $lends) {
        $this->lends[] = $lends;

        return $this;
    }

    /**
     * Remove lends
     *
     * @param Loan $lends
     */
    public function removeLend(Loan $lends) {
        $this->lends->removeElement($lends);
    }

    /**
     * Get lends
     *
     * @return Collection
     */
    public function getLends() {
        return $this->lends;
    }

    /**
     * Add groups
     *
     * @param GroupMember $groups
     * @return Member
     */
    public function addGroup(GroupMember $groups) {
        $this->groups[] = $groups;

        return $this;
    }

    /**
     * Remove groups
     *
     * @param GroupMember $groups
     */
    public function removeGroup(GroupMember $groups) {
        $this->groups->removeElement($groups);
    }

    /**
     * Get groups
     *
     * @return Collection
     */
    public function getGroups() {
        return $this->groups;
    }

    /**
     * Set admin
     *
     * @param boolean $admin
     * @return Member
     */
    public function setAdmin($admin) {
        $this->admin = $admin;
    }

    /**
     * Get admin
     *
     * @return boolean
     */
    public function getAdmin() {
        return $this->admin;
    }

    /**
     * Add ghostMembers
     *
     * @param GhostMember $ghostMembers
     * @return Member
     */
    public function addGhostMember(GhostMember $ghostMembers) {
        $this->ghostMembers[] = $ghostMembers;

        return $this;
    }

    /**
     * Remove ghostMembers
     *
     * @param GhostMember $ghostMembers
     */
    public function removeGhostMember(GhostMember $ghostMembers) {
        $this->ghostMembers->removeElement($ghostMembers);
    }

    /**
     * Get ghostMembers
     *
     * @return Collection
     */
    public function getGhostMembers() {
        return $this->ghostMembers;
    }

    /**
     * Add contacts
     *
     * @param Member $contacts
     * @return Member
     */
    public function addContact(Member $contacts)
    {
        $this->contacts[] = $contacts;

        return $this;
    }

    /**
     * Remove contacts
     *
     * @param Member $contacts
     */
    public function removeContact(Member $contacts)
    {
        $this->contacts->removeElement($contacts);
    }

    /**
     * Get contacts
     *
     * @return Collection
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * Add contactOf
     *
     * @param Member $contactOf
     * @return Member
     */
    public function addContactOf(Member $contactOf)
    {
        $this->contactOf[] = $contactOf;

        return $this;
    }

    /**
     * Remove contactOf
     *
     * @param Member $contactOf
     */
    public function removeContactOf(Member $contactOf)
    {
        $this->contactOf->removeElement($contactOf);
    }

    /**
     * Get contactOf
     *
     * @return Collection
     */
    public function getContactOf()
    {
        return $this->contactOf;
    }

    /**
     * Add documentMembers
     *
     * @param \src\Entity\DocumentMember $documentMembers
     * @return Member
     */
    public function addDocumentMember(\src\Entity\DocumentMember $documentMembers)
    {
        $this->documentMembers[] = $documentMembers;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getAuthorBans()
    {
        return $this->authorBans;
    }

    /**
     * @param ArrayCollection $authorBans
     * @return Member
     */
    public function setAuthorBans($authorBans)
    {
        $this->authorBans = $authorBans;
        return $this;
    }



    /**
     * Add authorBans
     *
     * @param \src\Entity\GroupBan $authorBans
     * @return Member
     */
    public function addAuthorBan(\src\Entity\GroupBan $authorBans)
    {
        $this->authorBans[] = $authorBans;

        return $this;
    }

    /**
     * Remove authorBans
     *
     * @param \src\Entity\GroupBan $authorBans
     */
    public function removeAuthorBan(\src\Entity\GroupBan $authorBans)
    {
        $this->authorBans->removeElement($authorBans);
    }

    /**
     * Add groupBans
     *
     * @param \src\Entity\GroupBan $groupBans
     * @return Member
     */
    public function addGroupBan(\src\Entity\GroupBan $groupBans)
    {
        $this->groupBans[] = $groupBans;

        return $this;
    }

    /**
     * Remove documentMembers
     *
     * @param \src\Entity\DocumentMember $documentMembers
     */
    public function removeDocumentMember(\src\Entity\DocumentMember $documentMembers)
    {
        $this->documentMembers->removeElement($documentMembers);
    }

    /**
     * Get documentMembers
     *
     * @retrun \Doctrine\Common\Collections\Collection
     */
     public function getDocumentMembers()
    {
        return $this->documentMembers;
    }

    /**
     * Add wishes
     *
     */
    public function addWish(\src\Entity\Wish $wishes)
    {
        $this->wishes[] = $wishes;
        return $this;
    }

    /**
     * Add activities
     *
     * @param \src\Entity\Activity $activities
     * @return Member
     */
    public function addActivity(\src\Entity\Activity $activities)
    {
        $this->activities[] = $activities;
        return $this;
    }

    /*
     * Set urlProfilPicture
     *
     * @param string $urlProfilPicture
     * @return Member
     */
    public function setUrlProfilPicture($urlProfilPicture) {
        $this->urlProfilPicture = $urlProfilPicture;
        return $this;
    }


    /**
     * Remove activities
     *
     * @param \src\Entity\Activity $activities
     */
    public function removeActivity(\src\Entity\Activity $activities)
    {
        $this->activities->removeElement($activities);
    }

    /**
     * Get activities
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getActivities()
    {
        return $this->activities;
    }

    /**
     * Remove wishes
     *
     * @param \src\Entity\Wish $wishes
     */
    public function removeWish(\src\Entity\Wish $wishes)
    {
        $this->wishes->removeElement($wishes);
    }

     /**
     * Remove groupBans
     *
     * @param \src\Entity\GroupBan $groupBans
     */
    public function removeGroupBan(\src\Entity\GroupBan $groupBans)
    {
        $this->groupBans->removeElement($groupBans);
    }


    /**
     * Get wishes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWishes()
    {
        return $this->wishes;

    }

    /*
     * Add authorComments
     *
     * @param \src\Entity\DocumentComment $authorComments
     * @return Member
     */
    public function addAuthorComment(\src\Entity\DocumentComment $authorComments) {
        $this->authorComments[] = $authorComments;
    }

    /**
     * Remove authorComments
     *
     * @param \src\Entity\DocumentComment $authorComments
     */
    public function removeAuthorComment(\src\Entity\DocumentComment $authorComments)
    {
        $this->authorComments->removeElement($authorComments);
    }

    /**
     * Get authorComments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAuthorComments() {
        return $this->authorComments;
    }

    /**
     * Get urlProfilPicture
     *
     * @return string
     */
    public function getUrlProfilPicture()
    {
        return $this->urlProfilPicture;
    }

    /**
     * Get groupBans
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGroupBans()
    {
        return $this->groupBans;
    }

    /**
     * Add authorPosts
     *
     * @param \src\Entity\GroupPost $authorPosts
     * @return Member
     */
    public function addAuthorPost(\src\Entity\GroupPost $authorPosts)
    {
        $this->authorPosts[] = $authorPosts;

        return $this;
    }

    /**
     * Remove authorPosts
     *
     * @param \src\Entity\GroupPost $authorPosts
     */
    public function removeAuthorPost(\src\Entity\GroupPost $authorPosts)
    {
        $this->authorPosts->removeElement($authorPosts);
    }

    /**
     * Get authorPosts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAuthorPosts()
    {
        return $this->authorPosts;
    }
}
