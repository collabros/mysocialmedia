app.controller('AlertCtrl', ['$scope', '$timeout', function ($scope, $timeout) {
    $scope.alerts = [];

    $scope.$on('alertEvent', function (event, alert) {
        var index = $scope.alerts.push({msg:alert.message, type:alert.type});
        $timeout(function () {
            $scope.closeAlert(index);
        }, 10000)
    });

    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index-1, 1);
    };

}]);