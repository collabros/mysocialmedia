app.controller('DashboardCtrl', ['$scope', '$location', 'loginService', '$http', '$routeParams', 'memberService', '$rootScope', '$timeout', 'mediaService', function ($scope, $location, loginService, $http, $routeParams, memberService, $rootScope, $timeout, mediaService) {

    $scope.myDocuments = [];

    memberService.getDashboardStatistics(memberService.getId())
        .success(function (result) {
            $scope.statistics = [];
            if (result.statistics.documents !== undefined) {
                $scope.statistics.push({name: "Documents", data: result.statistics.documents});
            }
            if (result.statistics.medias !== undefined) {
                $scope.statistics.push({name: "Médiathèques", data: result.statistics.medias});
            }
            if (result.statistics.contacts !== undefined) {
                $scope.statistics.push({name: "Contacts", data: result.statistics.contacts});
            }
            if (result.statistics.borrows !== undefined && result.statistics.lends !== undefined) {
                $scope.statistics.push({name: "Emprunts", data: (result.statistics.borrows + result.statistics.lends)});
            }
        });

    $scope.search = "";

    $scope.$watch('search', function(newVal, oldVal){
        if(newVal.length > 3) {
            searchService.documentSearch({search:$scope.search})
                .success(function (result) {
                    $scope.searchStarted = true;
                    for (var i = 0; i < result.documents.length; i++) {
                        result.documents[i].detail = rootpath+"/document/"+result.documents[i].id;
                    }
                    $scope.documents = result.documents;
                });
            searchService.userSearch({search:$scope.search})
                .success(function (result) {
                    $scope.searchStarted = true;
                    if(result.members != undefined) {
                        for (var i = 0; i < result.members.length; i++) {
                            result.members[i].detail = rootpath+"/member/"+result.members[i].id;
                        }
                    }
                    $scope.members = result.members;
                    $scope.ghosts = result.ghosts;
                });
            searchService.groupSearch({search:$scope.search})
                .success(function (result) {
                    $scope.searchStarted = true;
                    if(result.grous != undefined) {
                        for (var i = 0; i < result.groups.length; i++) {
                            result.groups[i].detail = rootpath+"/groups/"+result.groups[i].id;
                        }
                    }
                    $.groups = result.groups;
                });
        }
    }, true);

    memberService.getLatestMines(memberService.getId())
        .success(function (result) {
            $scope.myDocumentsLoaded = true;
            for (var i = 0; i < result.documents.length; i++) {
                result.documents[i].detail = rootpath + "/document/" + result.documents[i].id;
            }
            $scope.myDocuments = result.documents;
        });

    memberService.getLatestContacts(memberService.getId())
        .success(function (result) {
            $scope.contactsLoaded = true;
            for (var i = 0; i < result.contacts.length; i++) {
                result.contacts[i].detail = rootpath + "/member/" + result.contacts[i].id;
            }
            $scope.contacts = result.contacts;
        });

    mediaService.getAll()
        .success(function (result) {
            $scope.collections = result.medias;
        });

    memberService.getLatestLoans(memberService.getId())
        .success(function (result) {
            $scope.loans = result.loans;
        });

    memberService.getLatestComments(memberService.getId())
        .success(function(result){
            for(var i = 0; i < result.comments.length; i++){
                result.comments[i].document.detail = rootpath + "/document/" + result.comments[i].document.id;
            }
            $scope.latestComments = result.comments;
        });
}]);