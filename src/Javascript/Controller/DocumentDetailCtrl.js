app.controller('DocumentDetailCtrl', ['$scope', '$rootScope','$location', 'loginService', '$http', '$routeParams', 'documentService', 'memberService', 'mediaService','wishService',function ($scope, $rootScope,$location, loginService, $http, $routeParams, documentService, memberService, mediaService, wishService) {

    $scope.myDocuments = [];
    $scope.addDocument = rootpath + "/document/add";
    $scope.isIn = false;
    $scope.fullLoaded = false;

    $scope.authorId = memberService.getId();
    $scope.isAdmin = memberService.getAdmin();

    documentService.getOne($routeParams.id)
        .success(function (result) {
            $scope.document = result.documents;
            memberService.getMines(memberService.getId()).success(function(res){
                $scope.myDocumentsLoaded = true;
                for (var i = 0; i < res.documents.length; i++) {
                    res.documents[i].detail = rootpath + "/document/" + res.documents[i].id;
                    if(res.documents[i].id == $scope.document.id){
                        $scope.isIn = true;
                    }
                    $scope.myDocuments.push(res.documents[i]);
                }
                $scope.myDocuments.count = res.count;
                $scope.myDocuments.canLoad = (res.documents.length !== 0);
            });
            wishService.getAll(memberService.getId())
                .success(function(result){
                    $scope.wishlist = result.documents;
                    $scope.wishlistFullyLoaded = true;
                    for(i = 0;i<result.documents.length; i++){
                        if(result.documents[i].id == $scope.document.id){
                            $scope.document.inWishlist = true;
                        }
                    }
                    $scope.fullLoaded = true;
                }
            );

        });

    mediaService.getAll()
        .success(function (result) {
            $scope.collections = result.medias;
        });

    $scope.addToCollection = function (id) {
        memberService.addDocument(memberService.getId(), id)
            .success(function (result) {
                $scope.myDocuments.count++;
                $scope.myDocuments.push(result.documents);
                $scope.isIn = true;
                $rootScope.$broadcast('alertEvent', {message:"Document ajouté avec succès", type:"success"});
            })
            .error(function () {
                $rootScope.$broadcast('alertEvent', {message:"Vous possédez déjà ce document", type:"danger"});
            });
    };

    $scope.removeFromCollection = function (id) {
        memberService.removeDocument(memberService.getId(), id)
            .success(function () {
                for (var i = 0; i < $scope.myDocuments.length; i++) {
                    if($scope.myDocuments[i].id === id) {
                        $scope.myDocuments.count--;
                        $scope.myDocuments.splice(i, 1);
                        break;
                    }
                }
                $scope.isIn = false;
                $rootScope.$broadcast('alertEvent', {message:"Document supprimé avec succès", type:"success"});

            })
            .error(function (e) {
                $rootScope.$broadcast('alertEvent', {message: e.message, type:"danger"});
            });
    };

    $scope.deleteDocument = function (id) {
        documentService.deleteOne(id)
            .success(function () {
                for (var i = 0; i < $scope.documents.length; i++) {
                    if ($scope.documents[i].id == id) {
                        $scope.documents.splice(i, 1);
                    }
                    $scope.isIn = false;
                }
            });
    };

    $scope.toggleDocumentMedia = function (mediaId, docId, checked) {
        if(checked) {
            mediaService.addDocument(mediaId, docId)
        } else {
            mediaService.removeDocument(mediaId, docId)
        }
    };

    $scope.$watchCollection('collections', function (newVal) {
        if(newVal) {
            for (var i = 0; i < newVal.length; i++) {
                var collection = newVal[i];
                if(collection.documents) {
                    for (var j = 0; j < collection.documents.length; j++) {
                        if($scope.document.id === collection.documents[j].id) {
                            if($scope.document.collection === undefined) {
                                $scope.document.collection = [];
                            }
                            $scope.document.collection[collection.id] = true;
                        }
                    }
                }
            }
        }
    });

    $scope.addWish = function(id){
        wishService.addOne(memberService.getId(),{document:id})
            .success(function(){
                $rootScope.$broadcast('alertEvent', {message:"Document ajouté à vos souhaits", type:"success"});
                if($scope.documents !== undefined) {
                    for (var i = 0; i < $scope.documents.length; i++) {
                        if($scope.documents[i].id === id) {
                            $scope.inWishlist = true;
                        }
                    }
                }
                if($scope.latestDocuments !== undefined) {
                    for (var i = 0; i < $scope.latestDocuments.length; i++) {
                        if($scope.latestDocuments[i].id === id) {
                            $scope.inWishlist = true;
                        }
                    }
                }
                if($scope.document !== undefined){
                    $scope.document.inWishlist = true;
                }
            });
    };

    $scope.removeWish = function (id) {
        wishService.deleteOne(memberService.getId(), id)
            .success(function(){
                $rootScope.$broadcast('alertEvent', {message:"Document supprimé de vos souhaits", type:"success"});
                if($scope.documents !== undefined) {
                    for (var i = 0; i < $scope.documents.length; i++) {
                        if($scope.documents[i].id === id) {
                            $scope.documents[i].inWishlist = false;
                        }
                    }
                }
                if($scope.latestDocuments !== undefined) {
                    for (var i = 0; i < $scope.latestDocuments.length; i++) {
                        if($scope.latestDocuments[i].id === id) {
                            $scope.latestDocuments[i].inWishlist = false;
                        }
                    }
                }
                if($scope.document !== undefined){
                    $scope.document.inWishlist = false;
                }
            });
    };

    $scope.addComment = function(document_id) {
        if ($scope.newMessage && !$scope.addCommentWorking) {
            var comment = {
                "message": $scope.newMessage
            };
            $scope.addCommentWorking = true;
            documentService.addComment(document_id, comment)
                .success(function () {
                    $scope.newMessage = "";
                    $scope.addCommentWorking = false;
                    getLatestComments($routeParams.id);
                    $rootScope.$broadcast('alertEvent', {message:"Commentaire ajouté", type:"success"});
                });
        }
    };

    $scope.deleteComment = function (document_id, comment_id) {
        documentService.deleteComment(document_id, comment_id)
            .success(function () {
                for (var i = 0; i < $scope.comments.length; i++) {
                    if ($scope.comments[i].id == comment_id) {
                        $scope.comments.splice(i, 1);
                    }
                }
                $rootScope.$broadcast('alertEvent', {message:"Commentaire supprimé", type:"success"});
            })
            .error(function (result) {
                console.log(result);
            });
    };

    $scope.moreLatestComments = function () {
        getLatestComments($routeParams.id, $scope.comments.length);
    };

    getLatestComments = function(id, from) {
        var query = {};

        if (from === undefined) {
            query=documentService.getLatestComments(id);
        }
        else {
            query=documentService.getLatestComments(id, from);
        }
        query.success(function (result) {
            if (from === undefined) {
                $scope.comments = result.comments;
            }
            else {
                for (var i=0; i<result.comments.length; i++) {
                    $scope.comments.push(result.comments[i]);
                }
            }
            if (result.comments.length === 0) {
                $scope.comments.canLoad = false;
            } else {
                $scope.comments.canLoad = true;
            }
        });
    };
    getLatestComments($routeParams.id);

    $scope.editComment = function (document_id, comment_id, message) {

        var comment = {
            "message": message
        };
        documentService.editComment(document_id, comment_id, comment)
            .success(function () {
                getLatestComments($routeParams.id);
                $rootScope.$broadcast('alertEvent', {message:"Commentaire édité", type:"success"});
            })
            .error(function (result) {
                console.log(result);
            });
    };
}]);