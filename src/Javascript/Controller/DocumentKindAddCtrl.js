app.controller('DocumentKindAddCtrl', ['$scope', '$location', 'loginService', '$http', 'documentKindService', function ($scope, $location, loginService, $http, documentKindService) {

    $scope.title = "Créer un genre de document";

    $scope.submit = function () {
        documentKindService.addOne($scope.kind)
            .success(function () {
                $location.path('/document/kind');
            });
    };

}]);