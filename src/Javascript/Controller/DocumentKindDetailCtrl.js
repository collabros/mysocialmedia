app.controller('DocumentKindDetailCtrl', ['$scope', '$location', 'loginService', 'documentKindService', function ($scope, $location, loginService, documentKindService) {

    documentKindService.getOne($routeParams.id)
        .success(function (result) {
            $scope.type = result.types;
        });

}]);