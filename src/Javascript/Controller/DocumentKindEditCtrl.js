app.controller('DocumentKindEditCtrl', ['$scope', '$location', 'loginService', '$http', '$routeParams', 'documentKindService', function ($scope, $location, loginService, $http, $routeParams, documentKindService) {

    $scope.title = "Modifier un genre de document";

    documentKindService.getOne($routeParams.id)
        .success(function (result) {
            $scope.kind = result.kinds;
        });

    $scope.submit = function () {
        documentKindService.editOne($routeParams.id, $scope.kind)
            .success(function () {
                $location.path('/document/kind');
            });
    };

}]);