app.controller('DocumentTypeAddCtrl', ['$scope', '$location', 'loginService', '$http', 'documentTypeService', function ($scope, $location, loginService, $http, documentTypeService) {

    $scope.title = "Créer un type de document";

    $scope.submit = function () {
        documentTypeService.addOne($scope.type).
            success(function () {
                $location.path('/document/type');
            });
    };

}]);