app.controller('DocumentTypeCtrl', ['$scope', '$location', 'loginService', '$http', 'documentTypeService', function ($scope, $location, loginService, $http, documentTypeService) {

    $scope.createDocumentType = rootpath+"/document/type/add";

    documentTypeService.getAll()
        .success(function (result) {
            for (var i = 0; i < result.types.length; i++) {
                result.types[i].edit = rootpath+"/document/type/"+result.types[i].id+"/edit";
                result.types[i].delete = rootpath+"/document/type/"+result.types[i].id+"/delete";
            }
            $scope.types = result.types;
        });

}]);