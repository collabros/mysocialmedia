app.controller('DocumentTypeDeleteCtrl', ['$scope', '$location', 'loginService', '$http', '$routeParams', 'documentTypeService', function ($scope, $location, loginService, $http, $routeParams, documentTypeService) {

    $scope.title = "Supprimer un type de document ?";

    documentTypeService.getOne($routeParams.id)
        .success(function (result) {
            $scope.type = result.types;
            $scope.title = "Supprimer le type de document \""+$scope.type.name+"\" ?";
        });

    $scope.deleteType = function () {
        documentTypeService.deleteOne($routeParams.id)
            .success(function () {
                $location.path('/document/type');
            });
    };

    $scope.cancel = function () {
        $location.path('/document/type');
    }

}]);