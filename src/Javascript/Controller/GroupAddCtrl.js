app.controller('GroupAddCtrl', ['$scope', '$location', '$http', '$routeParams', 'loginService', 'groupService', function ($scope, $location, $http, $routeParams, loginService, groupService) {

    $scope.title = "Créer un groupe";

    var group = {};

    $scope.group = group;

    $scope.group.type = 0;

    groupService.getTypes()
        .success(function (result) {
            $scope.types=result.types;
        });



    $scope.submit = function() {
        $scope.addGroupWorking = true;
        groupService.add($scope.group)
            .success(function (result) {
                $scope.addGroupWorking = false;
                $location.path('/groups/' + result.group.id);
            });
    };

}]);