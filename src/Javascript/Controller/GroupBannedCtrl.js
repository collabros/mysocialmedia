app.controller('GroupBannedCtrl', ['$scope', '$location', '$http', '$routeParams', 'loginService', 'groupService', 'searchService', 'memberService', '$timeout', '$rootScope', '$filter', function ($scope, $location, $http, $routeParams, loginService, groupService, searchService, memberService, $timeout, $rootScope, $filter) {

    $scope.checkingRights = true;

    refreshGroup = function() {
        groupService.getOne($routeParams.id)
            .success(function (result) {
                result.groups.url = rootpath + "/groups/" + result.groups.id;
                $scope.group = result.groups;

                if ($scope.loggedMember === undefined || $scope.loggedMember.right.level > 2) {
                    $location.path(rootpath+"/social");
                    $rootScope.$broadcast('alertEvent', {message:"Vous ne pouvez pas accéder à ce contenu", type:"danger"});
                }
                else {
                    $scope.checkingRights = false;
                    getLatestMembers($routeParams.id);
                }
            });
    };


    refreshMember = function() {
        groupService.getOneMember($routeParams.id, memberService.getId())
            .success(function(result) {
                $scope.loggedMember = result.members;
                refreshGroup();
            })
            .error(function(result){
                refreshGroup();
            });
    };

    refreshMember();



    getLatestMembers = function(id, from) {
        var query = {};

        if (from === undefined) {
            query=groupService.getLatestBanned(id);
        }
        else {
            query=groupService.getLatestBanned(id, from);
        }
        query.success(function (result) {
            if (from === undefined) {
                for (var i = 0; i < result.members.length; i++) {
                    result.members[i].detail = rootpath + "/member/" + result.members[i].member.id;
                    result.members[i].edit = rootpath + "/groups/" + result.members[i].group.id + "/banned/" + result.members[i].id;
                }
                $scope.members = result.members;
            }
            else {
                for (var i = 0; i < result.members.length; i++) {
                    result.members[i].detail = rootpath + "/member/" + result.members[i].member.id;
                    result.members[i].edit = rootpath + "/groups/" + result.members[i].group.id + "/banned/" + result.members[i].id;
                    $scope.members.push(result.members[i]);
                }
            }
            $scope.members.canLoad = !(result.members.length === 0);
        });
    };

    getLatestMembers($routeParams.id);

    $scope.moreLatestMembers = function () {
        getLatestMembers($routeParams.id, $scope.members.length);
    };

    $scope.search = "";

    launchSearch = function () {
        var params = {};
        if ($scope.search.length > 3) {
            $scope.searchWorking = true;
            params.name=$scope.search;
            searchService.groupBannedSearch($routeParams.id, params)
                .success(function (result) {
                    $scope.searchWorking = false;
                    $scope.searchResults = {};
                    for (var i = 0; i < result.members.length; i++) {
                        result.members[i].detail = rootpath + "/member/" + result.members[i].member.id;
                        //From API side, inside a group, there is only one ban returned
                        if (result.members[i].bans.length !== 0) {
                            result.members[i].edit = rootpath + "/groups/" + $routeParams.id + "/banned/" + result.members[i].bans[0].id;
                        }
                        result.members[i].inGroup = false;
                        for (var j = 0; j < result.members[i].groups.length; j++) {
                            if (result.members[i].groups[j].id == $routeParams.id) {
                                result.members[i].inGroup = true;
                                result.members[i].role = result.members[i].groups[j].groupmember.right.name;
                                break;
                            }
                        }
                            //result.searchResults[i].detail = rootpath + "/document/" + result.documents[i].id;
                    }
                    $scope.searchResults = result.members;
                    $scope.searchResults.canLoad = (result.members.length !== 0);
                });
        }
    };

    $scope.moreSearchMembers = function() {
        var params={};
        params.name = $scope.search;
        params.from = $scope.searchResults.length;
        params.count = 10;
        searchService.groupBannedSearch($routeParams.id, params)
            .success(function(result) {
                if (result.members.length > 0) {
                    for(var i = 0; i<result.members.length; i++) {
                        $scope.searchResults.push(result.members[i]);
                    }
                }
                $scope.searchResults.canLoad = (result.members.length !== 0);
            });
    };

    var promise = $timeout(launchSearch, 1000);

    $scope.$watch('search', function () {
        $timeout.cancel(promise);
        promise = $timeout(launchSearch, 1000);
    }, true);

    $scope.banMember = function () {
        var params = {};

        params.member = $scope.newBan.member.id;

        if ($scope.newBan.message != "") {
            params.message = $scope.newBan.message;
        }

        if ($scope.newBan.ended_at != "") {
            params.ended_at = $filter('date')($scope.newBan.ended_at, 'HH:mm:ss dd-MM-yyyy');
        }
        $scope.banWorking = true;
        groupService.addBanned($routeParams.id, params)
            .success(function (result) {
                for (var i = 0; i < $scope.searchResults.length; i++) {
                    $scope.banWorking = false;
                    $scope.showBanModal = false;
                    if($scope.searchResults[i].member.id === params.member) {
                        $scope.searchResults[i].banned = true;
                        getLatestMembers($routeParams.id);
                        launchSearch();
                        break;
                    }
                }
            });
    };

    $scope.unbanMember = function (id) {
        groupService.deleteBanned($routeParams.id, id)
            .success(function (result) {
                for (var i = 0; i < $scope.members.length; i++) {
                    if($scope.members[i].id === id) {
                        $scope.members.splice(i, 1);
                        break;
                    }
                }

                if ($scope.searchResults !== undefined) {
                    for (i = 0; i < $scope.searchResults.length; i++) {
                        if($scope.searchResults[i].id === id) {
                            $scope.searchResults[i].banned = false;
                            break;
                        }
                    }
                }
            });
    };

    $scope.showBanModal = false;

    $scope.toggleNewBanModal = function(ban) {
        $scope.newBan = {
            "member": {}
        };
        $scope.newBan.member.id = ban.member.id;
        $scope.newBan.member.name = ban.member.name;
        $scope.newBan.message = "";
        $scope.newBan.ended_at = "";
        $scope.showBanModal = !$scope.showBanModal;
    };

    $scope.openDP = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.dpopened = true;
    };



}]);
