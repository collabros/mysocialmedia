app.controller('GroupCtrl', ['$scope', '$location', '$http', '$routeParams', 'loginService', 'groupService', 'memberService', '$rootScope', function ($scope, $location, $http, $routeParams, loginService, groupService, memberService, $rootScope) {

    $scope.checkingRights = true;

    $scope.edit = {};
    $scope.editPost = {};

    refreshGroup = function() {
        groupService.getOne($routeParams.id)
            .success(function (result) {
                result.groups.edit = rootpath+"/groups/"+result.groups.id+"/edit";
                result.groups.users = rootpath+"/groups/"+result.groups.id+"/members";
                result.groups.banned = rootpath+"/groups/"+result.groups.id+"/banned";
                result.groups.url = rootpath + "/groups/" + result.groups.id;
                $scope.group = result.groups;
                $scope.edit.type = $scope.group.type.id;
                $scope.edit.name = $scope.group.name;

                if (!$scope.group.type.selfjoinable && $scope.loggedMember === undefined) {
                    $location.path(rootpath+"/social");
                    $rootScope.$broadcast('alertEvent', {message:"Vous ne pouvez pas afficher ce groupe", type:"danger"});
                }
                else {
                    $scope.checkingRights = false;
                    getLatestPosts($routeParams.id);
                }
            });
    };

    refreshType = function() {
        groupService.getTypes()
            .success(function (result) {
                $scope.types=result.types;
                refreshGroup();
            });
    };


    refreshMember = function() {
        groupService.getOneMember($routeParams.id, memberService.getId())
            .success(function(result) {
                $scope.loggedMember = result.members;
                refreshType();
            })
            .error(function(result){
                refreshType();
            });
    };

    refreshMember();


    $scope.join = function() {
        groupService.addMember($routeParams.id)
            .success(function(result) {
                refreshMember();
            });
    };

    getLatestPosts = function(id, from) {
        var query = {};

        if (from === undefined) {
            query=groupService.getLatestPosts(id);
        }
        else {
            query=groupService.getLatestPosts(id, from);
        }
        query.success(function (result) {
            for (var i = 0; i<result.posts.length; i++) {
                result.posts[i].author.link = rootpath + "/member/" + result.posts[i].author.id;
            }
            if (from === undefined) {
                $scope.posts = result.posts;
            }
            else {
                for (var i=0; i<result.posts.length; i++) {
                    $scope.posts.push(result.posts[i]);
                }
            }
            if (result.posts.length === 0) {
                $scope.posts.canLoad = false;
            } else {
                $scope.posts.canLoad = true;
            }
        });
    };



    $scope.moreLatestPosts = function () {
        getLatestPosts($routeParams.id, $scope.posts.length);
    };


    $scope.deleteGroup = function (id) {
        $scope.deleteGroupWorking = true;
        $scope.showEditModal = false;
        groupService.deleteOne(id)
            .success(function () {
                $scope.deleteGroupWorking = false;
                $location.path('/social');
                $rootScope.$broadcast('alertEvent', {message:"Groupe supprimé avec succès", type:"success"});
            });
    };

    $scope.deletePost = function (group_id, post_id) {
        $scope.deletePostWorking = true;
        groupService.deletePost(group_id, post_id)
            .success(function () {
                $scope.deletePostWorking = false;
                $scope.showEditPostModal = false;
                for (var i = 0; i < $scope.posts.length; i++) {
                    if ($scope.posts[i].id == post_id) {
                        $scope.posts.splice(i, 1);
                    }
                }
                $rootScope.$broadcast('alertEvent', {message:"Post supprimé avec succès", type:"success"});
            })
            .error(function (result) {
                $rootScope.$broadcast('alertEvent', {message:"Erreur lors de la suppression du post", type:"danger"});
            });
    };

    $scope.addPost = function(group_id) {
        if ($scope.newMessage && !$scope.addPostWorking) {
            var post = {
                "message": $scope.newMessage
            };
            $scope.addPostWorking = true;
            groupService.addPost(group_id, post)
                .success(function () {
                    $scope.newMessage = "";
                    $scope.addPostWorking = false;
                    getLatestPosts($routeParams.id);
                })
                .error(function (result) {
                    $scope.addPostWorking = false;
                    $rootScope.$broadcast('alertEvent', {message:"Erreur lors de l'ajout du post", type:"danger"});
                });

        }
    };

    $scope.showEditModal = false;

    $scope.toggleEditModal = function () {
        if (!$scope.showEditModal) {
            $scope.edit.type = $scope.group.type.id;
            $scope.edit.name = $scope.group.name;
        }

        $scope.showEditModal = !$scope.showEditModal;
    };

    $scope.editGroup = function() {
        var newGroup = {
            "name": $scope.edit.name,
            "type": $scope.edit.type
        };
        $scope.editGroupWorking = true;
        groupService.editOne($routeParams.id, newGroup)
            .success(function(result) {
                $scope.editGroupWorking = false;
                $scope.showEditModal = false;
                refreshGroup();
                $rootScope.$broadcast('alertEvent', {message:"Groupe modifié avec succès", type:"success"});
            })
            .error(function(result) {
                $scope.editGroupWorking = false;
                $scope.showEditModal = false;
                $rootScope.$broadcast('alertEvent', {message:"Erreur lors de la modification du groupe", type:"danger"});
            });


    };

    $scope.toggleEditPostModal = function(post) {
        $scope.editPost.id = post.id;
        $scope.editPost.message = post.message;
        $scope.showEditPostModal = !$scope.showEditPostModal;
    };

    $scope.editPost = function() {
        if ($scope.editPost.message !== undefined && $scope.editPost.message !== "" ) {
            var newPost = {
                "message": $scope.editPost.message
            };
            $scope.editPostWorking = true;
            groupService.editPost($routeParams.id, $scope.editPost.id, newPost)
                .success(function(result) {
                    $scope.editPostWorking = false;
                    $scope.showEditPostModal = false;
                    getLatestPosts($routeParams.id);
                    $rootScope.$broadcast('alertEvent', {message:"Post modifié avec succès", type:"success"});
                })
                .error(function(result) {
                    $scope.editPostWorking = false;
                    $scope.showEditPostModal = false;
                    $rootScope.$broadcast('alertEvent', {message:"Erreur lors de la modification du post", type:"danger"});
                });
        }
    }

}]);