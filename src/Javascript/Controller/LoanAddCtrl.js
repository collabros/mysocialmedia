app.controller('LoanAddCtrl', ['$scope', '$location', 'loginService', '$http', 'documentTypeService', 'documentService', 'loanService', 'searchService', '$timeout','memberService', 'ghostMemberService',function ($scope, $location, loginService, $http, documentTypeService, documentService, loanService, searchService, $timeout,memberService,ghostMemberService) {

    $scope.loan = {
        status : "borrow",
        documents : []
    };

    $scope.members = [];

    $scope.search = "";

    $scope.filter = 0;

    $scope.ghostMembers = [];


    $scope.setSelect = false;

    $scope.submit = function () {
        if($scope.form.$valid !== true) {
            if(form.borrowerName.$valid !== true) {
                $scope.error = "Veuillez rentrer le nom de l'emprunteur ";
            } else if(form.lenderName.$valid !== true) {
                $scope.error = "Veuillez rentrer le nom du prêteur ";
            } else {
                $scope.error = "Erreur dans le formulaire ";
            }
            return;
        }
        if($scope.loan.documents.length == 0) {
            $scope.error = "Veuillez choisir au moins un livre";
            return;
        }
        if($scope.selected !== undefined){
            console.log($scope.selected)
            if($scope.loan.status == 'borrow'){
                $scope.loan.lender = $scope.selected;
            }else{
                $scope.loan.borrower = $scope.selected;
            }
        }
        loanService.addOne($scope.loan)
            .success(function (result) {
                console.log(result);
                $location.path('/loan');
            });
    };

    $scope.$watch('document.selected', function (newVal, oldVal) {
        if(newVal != undefined && newVal.id != undefined) {
            addToList(newVal.id);
        }
    });

    $scope.showSearch = function () {
        $scope.displaySearch = true;
    };

    launchSearch = function () {
        if ($scope.search.length > 3) {
            $scope.searchStarted = true;
            $scope.searchLaunched = true;
            searchService.documentSearch({search: $scope.search})
                .success(function (result) {
                    $scope.searchStarted = false;
                    $scope.documents = {};
                    for (var i = 0; i < result.documents.length; i++) {
                        result.documents[i].detail = rootpath + "/document/" + result.documents[i].id;

                        for (var j = 0; j < $scope.loan.documents.length; j++) {
                            if($scope.loan.documents[j].id === result.documents[i].id) {
                                result.documents[i].inLoan = true;
                            }
                        }
                    }
                    $scope.documents = result.documents;
                });
        }
    };

    launchSearchGhostMember = function() {
        if($scope.loan.borrower !== undefined){
            console.log($scope.loan.borrower.name);
            if($scope.loan.borrower.name.length > 3){
                $scope.searchGhostMemberStarted = true;
                $scope.searchGhostMemberLaunched = true;
                if($scope.status == 'lend'){
                    ghostMemberService.find($scope.loan.borrower.name)
                        .success(function(result) {
                            $scope.searchGhostMemberStarted = false;
                            $scope.ghostMembers = result.ghostMembers;
                        });
                }
            }
        }else{
            if($scope.loan.lender !== undefined){
                console.log($scope.loan.lender.name);
                if($scope.loan.lender.name.length > 3){
                    if($scope.loan.status == 'borrow'){
                        ghostMemberService.find($scope.loan.lender.name)
                            .success(function(result) {
                                $scope.searchGhostMemberStarted = false;
                                $scope.ghostMembers = result.ghostMembers;
                            });

                    }
                }
            }
        }
    };

    var promise = $timeout(launchSearch, 1000);
    var promiseGhostMember = $timeout(launchSearchGhostMember, 1000);


    $scope.$watch('search', function () {
        $timeout.cancel(promise);
        promise = $timeout(launchSearch, 1000);
    }, true);

    $scope.$watch('loan.borrower.name',function(){
        if(!$scope.setSelect){
            $timeout.cancel(promiseGhostMember);
            promiseGhostMember = $timeout(launchSearchGhostMember,1000);
            delete $scope.selected;
        }else{
            $scope.setSelect = false;
        }
    },true);
    $scope.$watch('loan.lender.name',function(){
        if(!$scope.setSelect){
            $timeout.cancel(promiseGhostMember);
            delete $scope.selected;
            promiseGhostMember = $timeout(launchSearchGhostMember,1000);
        }else{
            $scope.setSelect = false;
        }
    },true);

    $scope.addDocument = function (id) {
        for (var i = 0; i < $scope.loan.documents.length; i++) {
            if($scope.loan.documents[i].id === id) {
                return;
            }
        }
        for (i = 0; i < $scope.documents.length; i++) {
            if($scope.documents[i].id === id) {
                $scope.documents[i].inLoan = true;
                $scope.loan.documents.push($scope.documents[i]);
                break;
            }
        }
    };

    $scope.removeDocument = function (id) {
        for (var i = 0; i < $scope.loan.documents.length; i++) {
            if($scope.loan.documents[i].id === id) {
                $scope.loan.documents.splice(i, 1);
                break;
            }
        }

        if($scope.documents !== undefined) {
            for (var j = 0; j < $scope.documents.length; j++) {
                if($scope.documents[j].id === id) {
                    $scope.documents[j].inLoan = false;
                    break;
                }
            }
        }
    };

    $scope.select = function(member){
        $scope.selected = member;
        $scope.setSelect = true;
        $scope.ghostMembers = [];
        if($scope.loan.status == 'borrow'){
            $scope.loan.lender = member;
        }else{
            $scope.loan.borrower = member;
        }
    }

}]);