app.controller('LoanCtrl', ['$scope', '$rootScope', '$location', 'loginService', '$http', 'loanService', 'documentTypeService', function ($scope, $rootScope, $location, loginService, $http, loanService, documentTypeService) {

    $scope.addLoan = rootpath + "/loan/add";

    loanService.getAll()
        .success(function (result) {
            $scope.loans = result.loans;
        });

    $scope.addLoanFunc = function () {
        $location.path('/loan/add');
    };

    $scope.detailLoan = function (loanId) {
        $location.path('/loan/'+loanId);
    };

    documentTypeService.getAll()
        .success(function (result) {
            $scope.types = result.types;
        });

    $scope.toggleLoanDetail = function ($event, id) {
        for (var i = 0; i < $scope.loans.length; i++) {
            if($scope.loans[i].id === id) {
                if($scope.loans[i].showed === undefined || $scope.loans[i].showed == false) {
                    $scope.loans[i].showed = true;
                    var a = "";
                    for (var j = 0; j < $scope.loans[i].documents.length; j++) {
                        a += '<tr class="'+$scope.loans[i].id+'"><td colspan="2"></td><td>'+$scope.loans[i].documents[j].name+'</td><td>'+$scope.loans[i].documents[j].type+'</td><td></td></tr>'
                    }
                    $($event.target).closest('tr').after(a);
                } else {
                    $scope.loans[i].showed = false;
                    $('.'+$scope.loans[i].id).each(function () {
                        $(this).remove();
                    });
                    delete $scope.loans[i].showed;
                }
                break;
            }
        }
    };

    $scope.removeLoan = function (id) {
        loanService.deleteOne(id)
            .success(function () {
                for (var i = 0; i < $scope.loans.length; i++) {
                    if($scope.loans[i].id === id) {
                        $scope.loans.splice(i, 1);
                    }
                }

                $rootScope.$broadcast('alertEvent', {message:"Emprunt supprimé avec succès", type:"success"});
            });
    };

}]);