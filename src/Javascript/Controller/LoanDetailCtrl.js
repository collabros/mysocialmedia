app.controller('LoanDetailCtrl', ['$scope', '$location', 'loginService', '$http', '$timeout', '$routeParams', 'loanService', 'memberService', 'searchService', function ($scope, $location, loginService, $http, $timeout, $routeParams, loanService, memberService, searchService) {

    loanService.getOne($routeParams.id)
        .success(function (result) {
            $scope.loan = result.loans;
            if(result.loans.borrower !== undefined && result.loans.borrower.id === memberService.getId()) {
                $scope.loan.status = 'borrow';
                delete $scope.loan.borrower;
                if($scope.loan.ghostLender !== undefined) {
                    $scope.loan.lender = $scope.loan.ghostLender;
                }
            } else if(result.loans.lender !== undefined && result.loans.lender.id === memberService.getId()) {
                $scope.loan.status = 'lend';
                delete $scope.loan.lender;
                if($scope.loan.ghostBorrower !== undefined) {
                    $scope.loan.borrower = $scope.loan.ghostBorrower;
                }
            }
        });


    $scope.search = "";

    $scope.filter = 0;

    $scope.submit = function () {
        if($scope.form.$valid !== true) {
            if(form.borrowerName.$valid !== true) {
                $scope.error = "Veuillez rentrer le nom de l'emprunteur ";
            } else if(form.lenderName.$valid !== true) {
                $scope.error = "Veuillez rentrer le nom du prêteur ";
            } else {
                $scope.error = "Erreur dans le formulaire ";
            }
        }

        if($scope.loan.documents.length == 0) {
            $scope.error = "Veuillez choisir au moins un document";
            return;
        }

        loanService.editOne($routeParams.id, $scope.loan)
            .success(function () {
                $location.path('/loan');
            });
    };

    $scope.$watch('document.selected', function (newVal, oldVal) {
        if(newVal != undefined && newVal.id != undefined) {
            addToList(newVal.id);
        }
    });

    $scope.showSearch = function () {
        $scope.displaySearch = true;
    };

    launchSearch = function () {
        if ($scope.search.length > 3) {
            $scope.searchStarted = true;
            $scope.searchLaunched = true;
            searchService.documentSearch({search: $scope.search})
                .success(function (result) {
                    $scope.searchStarted = false;
                    $scope.documents = {};
                    for (var i = 0; i < result.documents.length; i++) {
                        result.documents[i].detail = rootpath + "/document/" + result.documents[i].id;

                        for (var j = 0; j < $scope.loan.documents.length; j++) {
                            if($scope.loan.documents[j].id === result.documents[i].id) {
                                result.documents[i].inLoan = true;
                            }
                        }
                    }
                    $scope.documents = result.documents;
                });
        }
    };

    var promise = $timeout(launchSearch, 1000);

    $scope.$watch('search', function () {
        $timeout.cancel(promise);
        promise = $timeout(launchSearch, 1000);
    }, true);

    $scope.addDocument = function (id) {
        for (var i = 0; i < $scope.loan.documents.length; i++) {
            if($scope.loan.documents[i].id === id) {
                return;
            }
        }
        for (i = 0; i < $scope.documents.length; i++) {
            if($scope.documents[i].id === id) {
                $scope.documents[i].inLoan = true;
                $scope.loan.documents.push($scope.documents[i]);
                break;
            }
        }
    };

    $scope.removeDocument = function (id) {
        for (var i = 0; i < $scope.loan.documents.length; i++) {
            if($scope.loan.documents[i].id === id) {
                $scope.loan.documents.splice(i, 1);
                break;
            }
        }

        if($scope.documents !== undefined) {
            for (var j = 0; j < $scope.documents.length; j++) {
                if($scope.documents[j].id === id) {
                    $scope.documents[j].inLoan = false;
                    break;
                }
            }
        }
    };

    $scope.deleteLoan = function () {
        loanService.deleteOne($routeParams.id)
            .success(function () {
                $location.path('/loan');
            });
    };

}]);