app.controller('LoginCtrl', ['$scope', '$rootScope', '$location', 'loginService', function ($scope, $rootScope, $location, loginService) {

    $scope.login = function () {
        if($scope.form.$valid !== true) {
            $rootScope.$broadcast('alertEvent', {message: "Le formulaire n'est pas valide", type:"danger"});
            return;
        }

        loginService.login($scope.user, $scope)
            .success(function (result) {
                localStorage.setItem('infos', JSON.stringify(result.infos));
                $location.path('/dashboard');
            })
            .error(function (e) {
                $rootScope.$broadcast('alertEvent', {message: e.message, type:"danger"});
            });
    };

    $scope.register = function () {
        $location.path('/register')
    };

}]);