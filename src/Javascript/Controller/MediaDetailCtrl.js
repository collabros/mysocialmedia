app.controller('MediaDetailCtrl', ['$scope', '$location', '$http', '$routeParams', 'loginService', 'mediaService', 'searchService', '$timeout', function ($scope, $location, $http, $routeParams, loginService, mediaService, searchService, $timeout) {

    $scope.search = "";

    $scope.filter = 0;

    $scope.maxlength = 50;

    launchSearch = function () {
        if ($scope.search.length > 3) {
            $scope.searchStarted = true;
            $scope.searchLaunched = true;
            searchService.documentSearch({search: $scope.search})
                .success(function (result) {
                    $scope.searchStarted = false;
                    $scope.documents = {};
                    for (var i = 0; i < result.documents.length; i++) {
                        result.documents[i].detail = rootpath + "/document/" + result.documents[i].id;
                        for (var j = 0; j < $scope.media.documents.length; j++) {
                            if($scope.media.documents[j].id === result.documents[i].id) {
                                result.documents[i].inCollection = true;
                            }
                        }
                    }
                    $scope.documents = result.documents;
                });
        }
    };

    var promise = $timeout(launchSearch, 1000);

    $scope.$watch('search', function () {
        $timeout.cancel(promise);
        promise = $timeout(launchSearch, 1000);
    }, true);

    $scope.addDocument = function (id) {
        for (var i = 0; i < $scope.media.documents.length; i++) {
            if($scope.media.documents[i].id === id) {
                return;
            }
        }

        mediaService.addDocument($routeParams.id, id)
            .success(function () {
                for (i = 0; i < $scope.documents.length; i++) {
                    if($scope.documents[i].id === id) {
                        $scope.documents[i].inCollection = true;
                        $scope.media.documents.push($scope.documents[i]);
                        break;
                    }
                }
            });
    };

    $scope.removeDocument = function (id) {
        mediaService.removeDocument($routeParams.id, id)
            .success(function () {
                for (var i = 0; i < $scope.media.documents.length; i++) {
                    if($scope.media.documents[i].id === id) {
                        $scope.media.documents.splice(i, 1);
                        break;
                    }
                }

                if($scope.documents !== undefined) {
                    for (var j = 0; j < $scope.documents.length; j++) {
                        if($scope.documents[j].id === id) {
                            $scope.documents[j].inCollection = false;
                            break;
                        }
                    }
                }
            });
    };

    $scope.setPublic = function () {
        mediaService.setPublic($routeParams.id)
            .success(function () {
                $scope.media.public = true;
            });
    };

    $scope.setPrivate = function () {
        mediaService.setPrivate($routeParams.id)
            .success(function () {
                $scope.media.public = false;
            });
    };

    $scope.editName = function () {
        $scope.inputEditName = true;
    };

    $scope.setName = function () {
        mediaService.setName($routeParams.id, $scope.media.name)
            .success(function () {
                $scope.inputEditName = false;
            });
    };

    $scope.showSearch = function () {
        $scope.displaySearch = true;
    };

    mediaService.getOne($routeParams.id)
        .success(function (result) {
            result.medias.edit = rootpath+"/media/"+result.medias.id+"/edit";
            for (var i = 0; i < result.medias.documents.length; i++) {
                result.medias.documents[i].detail = rootpath + "/document/" + result.medias.documents[i].id;
            }
            $scope.media = result.medias;
        });

}]);