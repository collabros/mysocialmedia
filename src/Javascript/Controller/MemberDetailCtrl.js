app.controller('MemberDetailCtrl', ['$scope', '$rootScope', '$location', '$http', '$routeParams', 'loginService', 'memberService', function ($scope, $rootScope, $location, $http, $routeParams, loginService, memberService) {

    memberService.getOne($routeParams.id)
        .success(function (result) {
            $scope.member = result.members;

            memberService.getAllContacts(memberService.getId())
                .success(function (result) {
                    if($routeParams.id == memberService.getId()) {
                        $scope.member.itsMe = true;
                        return;
                    }
                    for (var i = 0; i < result.contacts.length; i++) {
                        if(result.contacts[i].id == $routeParams.id) {
                            $scope.member.inContacts = true;
                            break;
                        }
                    }
                });
        });

    $scope.visitMedia = function () {
        $location.path('/dcoument/'+$routeParams.id+'/media');
    };

    memberService.getPublicStatistics($routeParams.id)
        .success(function (result) {
            $scope.statistics = {};
            $scope.statistics.medias = result.statistics.medias;
            $scope.statistics.documents = result.statistics.documents;
            $scope.statistics.contacts = result.statistics.contacts;
        });

    memberService.getPublicMedias($routeParams.id)
        .success(function (result) {
            for (var i = 0; i < result.medias.length; i++) {
                result.medias[i].detail = rootpath+"/media/"+result.medias[i].id;
            }
            $scope.medias = result.medias;
        });

    $scope.removeContact = function () {
        memberService.removeContact($routeParams.id, memberService.getId())
            .success(function () {
                $scope.member.inContacts = false;
                $rootScope.$broadcast('alertEvent', {message:"Membre supprimé de vos contacts", type:"success"});
            });
    };

    $scope.addContact = function () {
        memberService.addContact($routeParams.id, memberService.getId())
            .success(function () {
                $scope.member.inContacts = true;
                $rootScope.$broadcast('alertEvent', {message:"Membre ajouté à vos contacts", type:"success"});
            });
    };
}]);