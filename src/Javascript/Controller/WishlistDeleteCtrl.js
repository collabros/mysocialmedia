app.controller('WishlistDeleteCtrl',['$scope', '$http', '$location','$routeParams','loginService','wishService', function ($scope, $http, $location,$routeParams,loginService,wishService) {

    $scope.title = "Supprimer un document de votre Wishlist ?";

    wishService.getOne($routeParams.memberid,$routeParams.documentid)
        .success(function(result){
            console.log(result);
            $scope.wish = result.wishes[0];
            $scope.title = "Supprimer \""+$scope.wish.name+"\" de votre wishlist ?";
        });

    $scope.deleteWish = function(){
        wishService.deleteOne($routeParams.memberid,$routeParams.documentid)
            .success(function(){
                $location.path('/wishlist/'+$routeParams.memberid);
            });
    };

    $scope.cancel = function(){
        $location.path('/wishlist/'+$routeParams.memberid+"/documents");
    }


}]);