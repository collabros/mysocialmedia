app.factory('documentKindService', ['$http', '$location', function($http, $location) {
    return {
        getAll: function() {
            return $http.get(rootpath+'/api/documents/kinds');
        },
        getOne: function (id) {
            return $http.get(rootpath+'/api/documents/kinds/'+id);
        },
        addOne: function (kind) {
            return $http.post(rootpath+'/api/documents/kinds/add', kind);
        },
        editOne: function (id, kind) {
            return $http.post(rootpath+'/api/documents/kinds/'+id+'/edit', kind);
        },
        deleteOne: function (id) {
            return $http.delete(rootpath+'/api/documents/kinds/'+id+'/delete');
        }

    }
}]);