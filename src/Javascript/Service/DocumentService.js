app.factory('documentService', ['$http', '$location', function($http, $location) {
    return {
        getAll: function() {
            return $http.get(rootpath+"/api/documents");
        },
        getLatest: function(from, count) {
            var param = {};
            if(from !== undefined) {
                param.from = from;
            }
            if(count !== undefined) {
                param.count = count;
            }

            return $http.post(rootpath+"/api/documents/latest", param);
        },
        getOne: function(id) {
            return $http.get(rootpath+'/api/documents/'+id);
        },
        editOne: function(id, document) {
            return $http.post(rootpath+'/api/documents/'+id+'/edit', document);
        },
        addAmazonDocument: function (asin) {
            return $http.post(rootpath + "/api/documents/search/asin/" + asin);
        },
        getLatestComments: function (id, from, count) {
            var param = {};
            if(from !== undefined) {
                param.from = from;
            }
            if(count !== undefined) {
                param.count = count;
            }

            return $http.post(rootpath + "/api/documents/" + id + "/comments/latest", param);
        },
        deleteComment: function(id_document, id_comment) {
            return $http.delete(rootpath + "/api/documents/" + id_document + "/comments/" + id_comment);
        },
        addComment: function(id_document, comment) {
            return $http.post(rootpath + "/api/documents/" + id_document + "/comments/add", comment);
        },
        editComment: function(id_document,id_comment, comment) {
            return $http.post(rootpath + "/api/documents/" + id_document + "/comments/" + id_comment, comment);
        }

    }
}]);