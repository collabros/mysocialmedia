app.factory('documentTypeService', ['$http', '$location', function($http, $location) {
    return {
        getAll: function() {
            return $http.get(rootpath+"/api/documents/types");
        },
        getOne: function (id) {
            return $http.get(rootpath+'/api/documents/types/'+id);
        },
        addOne: function (type) {
            return $http.post(rootpath+'/api/documents/types/add', type);
        },
        editOne: function (id, type) {
            return $http.post(rootpath+'/api/documents/types/'+id+'/edit', type);
        },
        deleteOne: function (id) {
            return $http.delete(rootpath+'/api/documents/types/'+id+'/delete');
        }

    }
}]);