app.factory('ghostMemberService', ['$http', '$location', '$rootScope','memberService', function($http, $location, $rootScope,memberService) {
    return {
        getOne: function (id) {
            return $http.get(rootpath+'/api/members/'+id);
        },
        find: function(name){
            return $http.get(rootpath+'/api/members/ghostmembers/search',{
                params: {
                    name:name,
                    idmember:memberService.getId()
                }
            });
        }
    }
}]);