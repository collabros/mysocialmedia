app.factory('loginService', ['$http', '$location', '$timeout', '$rootScope', 'memberService', function($http, $location, $timeout, $rootScope, memberService) {
    return {
        login: function(data, scope) {
            return $http.post(rootpath+'/api/members/login', data);
        },
        register: function(data, scope) {
            return $http.post(rootpath+'/api/members/register', data);
        },
        logout: function() {
            $http.post(rootpath+'/api/members/logout')
                .then(function() {
                    $rootScope.connected = false;
                    localStorage.removeItem('infos');
                    delete $rootScope.connected;

                    $timeout(function () {
                        $location.path(rootpath);
                    }, 1000)
                }, function () {
                    $location.path(rootpath);
                });
        },
        isLogged: function() {
            return $http.get(rootpath+'/api/members/session');
        }
    }
}]);

