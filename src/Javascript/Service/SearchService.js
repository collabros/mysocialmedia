app.factory('searchService', ['$http', '$location', function($http, $location) {
    return {
        documentSearch: function(search) {
            return $http.get(rootpath+'/api/documents/search', {params:search});
        },
        amazonSearch: function(search) {
            return $http.get(rootpath+'/api/documents/search/amazon', {params:search});
        },
        userSearch: function(search) {
            return $http.get(rootpath+'/api/members/search', {params:search});
        },
        groupSearch: function(search) {
            return $http.get(rootpath+'/api/groups/search', {params:search});
        },
        groupMemberSearch: function(group_id, search) {
            return $http.post(rootpath + '/api/groups/' + group_id + '/members/search', search);
        },
        groupBannedSearch: function(group_id, search) {
            return $http.post(rootpath + "/api/groups/" + group_id + "/banned/search", search);
        }
    }
}]);