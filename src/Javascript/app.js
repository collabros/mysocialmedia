var app = angular.module('app', ['ngRoute', 'ngSanitize', 'ngAnimate', 'ui.bootstrap']);

app.config(['$routeProvider', '$locationProvider', '$animateProvider', function($routeProvider, $locationProvider, $animateProvider) {

    $routeProvider
        .when('/home', {templateUrl:'src/Template/home.html', controller:'HomeCtrl'})
        .when('/login', {templateUrl:'src/Template/login.html', controller:'LoginCtrl'})
        .when('/oauth/signin/twitter', {templateUrl:'src/Template/dashboard.html', controller:'LoginTwitterCtrl'})
        .when('/register', {templateUrl:'src/Template/register.html', controller:'RegisterCtrl'})
        .when('/logout', {templateUrl:'src/Template/logout.html', controller:'LogoutCtrl'})
        .when('/dashboard', {templateUrl:'src/Template/dashboard.html', controller:'DashboardCtrl'})
        .when('/profile', {templateUrl:'src/Template/profile.html', controller:'ProfileCtrl', reloadOnSearch:false})
        .when('/contact', {templateUrl:'src/Template/contact.html', controller:'ContactCtrl'})
        .when('/social', {templateUrl:'src/Template/social.html', controller:'SocialCtrl'})
        .when('/media', {templateUrl:'src/Template/media.html', controller:'MediaCtrl'})
        .when('/media/add', {templateUrl:'src/Template/media-add.html', controller:'MediaAddCtrl'})
        .when('/media/:id', {templateUrl:'src/Template/media-detail.html', controller:'MediaDetailCtrl'})
        .when('/member', {templateUrl:'src/Template/member.html', controller:'MemberCtrl'})
        .when('/member/:id', {templateUrl:'src/Template/member-detail.html', controller:'MemberDetailCtrl'})
        .when('/document', {templateUrl:'src/Template/document.html', controller:'DocumentCtrl', reloadOnSearch:false})
        .when('/document/add', {templateUrl:'src/Template/document-edit.html', controller:'DocumentAddCtrl'})
        .when('/document/:id', {templateUrl:'src/Template/document-detail.html', controller:'DocumentDetailCtrl'})
        .when('/loan', {templateUrl:'src/Template/loan.html', controller:'LoanCtrl'})
        .when('/loan/add', {templateUrl:'src/Template/loan-add.html', controller:'LoanAddCtrl'})
        .when('/loan/:id', {templateUrl:'src/Template/loan-detail.html', controller:'LoanDetailCtrl'})
        .when('/loan/:id/edit', {templateUrl:'src/Template/loan-edit.html', controller:'LoanEditCtrl'})
        .when('/wishlist',{templateUrl:'src/Template/wishlist.html',controller:'WishlistCtrl'})
        .when('/groups/add', {templateUrl:'src/Template/group-add.html', controller:'GroupAddCtrl'})
        .when('/groups/:id', {templateUrl:'src/Template/group-detail.html', controller:'GroupCtrl'})
        .when('/groups/:id/members', {templateUrl:'src/Template/group-members.html', controller:'GroupMembersCtrl'})
        .when('/groups/:id/banned', {templateUrl:'src/Template/group-banned.html', controller:'GroupBannedCtrl'})
        .otherwise({redirectTo: '/home'});

    $locationProvider.html5Mode(true);

    $animateProvider.classNameFilter(/^((?!(fa-spin)).)*$/);
}]);

app.run(['$rootScope', '$route', '$location', 'loginService', '$routeParams', 'memberService', function($rootScope, $route, $location, loginService, $routeParams, memberService) {

    var visitorPermission = [
        {route:'/login'},
        {route:'/register'}
    ];

    var userPermission = [
        {route:'/dashboard'},
        {route:'/logout'},
        {regexp:'^/document*.'},
        {regexp:'^/loan*.'},
        {regexp:'^/contact*.'},
        {regexp:'^/media*.'},
        {regexp:'^/profile*.'},
        {regexp:'^/member*.'},
        {regexp:'^/social*.'},
        {regexp:'^/wishlist*.'},
        {regexp:'^/groups*.'}
    ];

    $rootScope.$on('$routeChangeStart', function () {
        var checkConnexion = null;
        var cond = false;
        var i = 0;
        var regexp = null;

        for (i = 0; i < userPermission.length; i++) {
            cond = false;
            if(userPermission[i].regexp != undefined) {
                regexp = new RegExp(userPermission[i].regexp, "i");
                cond = (regexp.test($location.path()) === true);
            } else if(userPermission[i].route != undefined) {
                cond = (userPermission[i].route === $location.path());
            }
            if(cond) {
                loginService.isLogged()
                    .then(function() {
                        memberService.rootInfo($location.path());
                        console.log("User rights checked");
                    }, function () {
                        $location.path('/login');
                    });
            }
        }

        for (i = 0; i < visitorPermission.length; i++) {
            cond = false;
            if(visitorPermission[i].regexp != undefined) {
                regexp = new RegExp(visitorPermission[i].regexp, "i");
                cond = (regexp.test($location.path()) === true);
            } else if(visitorPermission[i].route != undefined) {
                cond = (visitorPermission[i].route === $location.path());
            }
            if(cond) {
                loginService.isLogged()
                    .then(function() {
                        console.log("Visitor rights checked");
                        $location.path('/dashboard');
                    }, function () {

                    });
            }
        }

    });

    $rootScope.maxlength = 30;

}]);

app.directive('myDocumentCard', ['$http', 'mediaService', function ($http, mediaService) {

    return {
        templateUrl:'src/Template/Directive/my-document-card.html',
        link: function (scope, element, attrs) {
            scope.$watchCollection('collections', function (newVal) {
                if(newVal) {
                    scope.document.collections = [];
                    for (var i = 0; i < newVal.length; i++) {
                        var collection = newVal[i];
                        if(collection.documents) {
                            scope.document.collections[collection.id] = {added: false, onLoad: false};

                            for (var j = 0; j < collection.documents.length; j++) {
                                if(scope.document.id === collection.documents[j].id) {
                                    scope.document.collections[collection.id].added = true;
                                }
                            }
                        }
                    }
                }
            });


            scope.toggleDocumentMedia = function (doc, mediaId) {
                doc.collections[mediaId].onLoad = true;
                if(doc.collections[mediaId].added == false) {
                    mediaService.addDocument(mediaId, doc.id)
                        .success(function () {
                            doc.collections[mediaId].added = true;
                        })
                        .error(function () {
                            doc.collections[mediaId].added = false;
                        })
                        .then(function () {
                            doc.collections[mediaId].onLoad = false;
                        });
                } else {
                    mediaService.removeDocument(mediaId, doc.id)
                        .success(function () {
                            doc.collections[mediaId].added = false;
                        })
                        .error(function () {
                            doc.collections[mediaId].added = true;
                        })
                        .then(function () {
                            doc.collections[mediaId].onLoad = false;
                        });
                }
            };
        }
    };

}]);

app.directive('latestDocumentCard', ['$http', 'mediaService', function ($http, mediaService) {

    return {
        templateUrl:'src/Template/Directive/latest-document-card.html',
        link: function (scope, element, attrs) {
            scope.$watchCollection('collections', function (newVal) {
                if(newVal) {
                    for (var i = 0; i < newVal.length; i++) {
                        var collection = newVal[i];
                        if(collection.documents) {
                            for (var j = 0; j < collection.documents.length; j++) {
                                if(scope.document.id === collection.documents[j].id) {
                                    if(scope.document.collection === undefined) {
                                        scope.document.collection = [];
                                    }
                                    scope.document.collection[collection.id] = true;
                                }
                            }
                        }
                    }
                }
            });


            scope.toggleDocumentMedia = function (mediaId, docId, checked) {
                if(checked) {
                    mediaService.addDocument(mediaId, docId)
                } else {
                    mediaService.removeDocument(mediaId, docId)
                }
            };
        }
    };

}]);

app.directive('selectSearch', function () {
    return {
        templateUrl:'src/Template/Directive/select-search.html'
    };
});

app.directive('documentBase', function () {
    return {
        templateUrl:'src/Template/Directive/document-base.html'
    };
});

app.directive('searchDocumentCard', function () {
    return {
        templateUrl:'src/Template/Directive/search-document-card.html'
    };
});

app.directive('myMediaCard', function () {
    return {
        templateUrl:'src/Template/Directive/my-media-card.html'
    };
});

app.directive('bookmarkDropdown', function () {
    return {
        templateUrl:'src/Template/Directive/bookmark-dropdown.html'
    };
});

app.directive('amazonDocumentCard', function () {
    return {
        templateUrl:'src/Template/Directive/amazon-document-card.html'
    };
});

app.directive('addMediaCard', function () {
    return {
        templateUrl:'src/Template/Directive/add-media-card.html'
    };
});

app.directive('oauthButtons', function () {
    return {
        templateUrl:'src/Template/Directive/oauth-buttons.html'
    };
});

app.directive('addDocumentCard', function () {
    return {
        templateUrl:'src/Template/Directive/add-document-card.html'
    };
});

app.directive('searchMediaDocumentCard', function () {
    return {
        templateUrl:'src/Template/Directive/search-media-document-card.html'
    };
});

app.directive('searchLoanDocumentCard', function () {
    return {
        templateUrl:'src/Template/Directive/search-loan-document-card.html'
    };
});

app.directive('myMediaDocumentCard', function () {
    return {
        templateUrl:'src/Template/Directive/my-media-document-card.html'
    };
});

app.directive('myLoanDocumentCard', function () {
    return {
        templateUrl:'src/Template/Directive/my-loan-document-card.html'
    };
});

app.directive('mediaDocumentCard', function () {
    return {
        templateUrl:'src/Template/Directive/media-document-card.html'
    };
});

app.directive('publicMediaCard', function () {
    return {
        templateUrl:'src/Template/Directive/public-media-card.html'
    };
});

app.directive('contactBase', function () {
    return {
        templateUrl:'src/Template/Directive/contact-base.html'
    };
});

app.directive('myContactCard', function () {
    return {
        templateUrl:'src/Template/Directive/my-contact-card.html'
    };
});

app.directive('dashboardContactCard', function () {
    return {
        templateUrl:'src/Template/Directive/dashboard-contact-card.html'
    };
});

app.directive('addContactCard', function () {
    return {
        templateUrl:'src/Template/Directive/add-contact-card.html'
    };
});

app.directive('wishlistButton', function () {
    return {
        templateUrl:'src/Template/Directive/wishlist-button.html'
    };
});

app.directive('documentMenuDropdown', function () {
    return {
        templateUrl:'src/Template/Directive/document-menu-dropdown.html'
    };
});

app.directive('documentMenuDropdown', function () {
    return {
        templateUrl:'src/Template/Directive/document-menu-dropdown.html'
    };
});

app.directive('myLoanResumeCard', function () {
    return {
        templateUrl:'src/Template/Directive/my-loan-resume-card.html'
    };
});

app.directive('myWishDocumentCard', function () {
    return {
        templateUrl:'src/Template/Directive/my-wish-document-card.html'
    };
});

app.directive('contactCardLoan', function () {
    return {
        templateUrl:'src/Template/Directive/contact-card-loan.html'
    }
});

app.directive('latestCommentCard',function(){
    return {
        templateUrl:'src/Template/Directive/latest-comment-card.html'
    }
});

app.directive('addGroupCard',function(){
    return {
        templateUrl:'src/Template/Directive/add-group-card.html'
    }
});

app.directive('groupCard',function(){
    return {
        templateUrl:'src/Template/Directive/group-card.html'
    }
});

app.directive('modal', function () {
    return {
        template: '<div class="modal fade">' +
            '<div class="modal-dialog">' +
                '<div class="modal-content">' +
                    '<div class="modal-header">' +
                        '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                        '<p class="modal-title"><b ng-bind="amazonDetail.name"></b></p>' +
                    '</div>' +
                    '<div class="modal-body" ng-transclude></div>' +
                '</div>' +
            '</div>' +
        '</div>',
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: true,
        link: function postLink(scope, element, attrs) {
            scope.title = attrs.title;

            scope.$watch(attrs.visible, function (value) {
                if (value == true)
                    $(element).modal('show');
                else
                    $(element).modal('hide');
            });

            $(element).on('shown.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = true;
                });
            });

            $(element).on('hidden.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = false;
                });
            });
        }
    };
});

app.directive('tooltip', function(){
    return {
        restrict: 'A',
        link: function(scope, element, attrs){
            $(element).hover(function(){
                $(element).tooltip('show');
            }, function(){
                //$(element).tooltip('hide');
            });
        }
    };
});
