//Javascript for the home page
if (!$) {
    var $ = jQuery;
}

$(document).ready(function() {

    //function for determining if an element is visible on the screen
    function isVisible(el) {
        if (el instanceof jQuery) {
            el = el[0];
        }
        var rect = el.getBoundingClientRect();

        return (
        rect.bottom >= 0 &&
        rect.top <= (window.innerHeight || document.documentElement.clientHeight)
        );
    }

    if (window.addEventListener) window.addEventListener('DOMMouseScroll', function () {
    });
    window.onmousewheel = document.onmousewheel = function () {
    };

    // Cache the Window object
    $window = $(window);
    var isMobile = ( /Android|webOS|iPhone|iPad|iPod|Silk|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) );

    var winHeight = (window.innerHeight || document.documentElement.clientHeight);
    $(window).resize(function () {
        winHeight = (window.innerHeight || document.documentElement.clientHeight);
    });
    //parallax effect on learn more
    $('.header,.img-panel').each(function () {

        if (isMobile) {
            return;
        }
        var imagePanel = $(this);

        // run it the first time to initialize the position
        parrallaxOnScroll(imagePanel);

        $window.scroll(function () {
            parrallaxOnScroll(imagePanel);
        });

    });


    function parrallaxOnScroll(imagePanel) {
        // Move the background if the panel is visible
        if (isVisible(imagePanel)) {
            var panelHeight = imagePanel.height();
            var midHeight = imagePanel[0].getBoundingClientRect().top
                + (panelHeight / 2);

            var midPercentage = (midHeight / winHeight) * 100;

            //lets tollerate 50% move up and down of the background image once parts of the image are off screen
            if (midPercentage > 150) {
                midPercentage = 150
            } else if (midPercentage < -50) {
                midPercentage = -50;
            }

            // Put together our final background position
            var coords = '50% ' + midPercentage + '%';

            imagePanel.css('background-position', coords);

        }
    }

});