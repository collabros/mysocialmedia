<?php

namespace src\View;

use src\AbstractView;

class Index extends AbstractView {

    public function __construct() {
        $this->layout = "index.html.twig";
    }

}